<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Store extends MY_Controller {

	public function index()
    {
        
       $this->load->view($this->theme . 'front/store', $this->data);
    }

    public function detail()
    {
        
       $this->load->view($this->theme . 'front/detail', $this->data);
    }



}
