<?php defined('BASEPATH') OR exit('No direct script access allowed');


class Api extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
    }

    function news()
    {
        $news =$this->db->order_by('id','desc');
        $news =$this->db->get('sma_news')->result();
        $arr = array();
        foreach($news as $row)
        {
            $arr[] = array(
                    'id' => $row->id,
                    'date' => $row->datetime,
                    'title' => $row->title,
                    'category' => $row->category,
                    'author' => $row->author,
                    'image' => $row->picture
            );
        }
        echo json_encode($arr);
    }
    
    function news_detail()
    {
        $news =$this->db->query("SELECT * FROM sma_news WHERE id='".$this->uri->segment(3)."'")->row();
        $arr = array();
        $arr[] = array(
                    'id' => $news->id,
                    'date' => $news->datetime,
                    'title' => $news->title,
                    'text' => $news->text,
                    'category' => $news->category,
                    'author' => $news->author
        );
        echo json_encode($arr);
    }
    
    function product()
    {
        $news =$this->db->query("SELECT * FROM sma_products WHERE id='".$this->uri->segment(3)."'")->row();
        $arr = array();
        $arr[] = array(
                    'id' => $news->id,
                    'date' => $news->datetime,
                    'title' => $news->title,
                    'text' => $news->text,
                    'category' => $news->category,
                    'author' => $news->author
        );
        echo json_encode($arr);
    }

    
}