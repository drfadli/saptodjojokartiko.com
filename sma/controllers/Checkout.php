<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Checkout extends MY_Controller {


	public function index()
	{
		$this->load->view($this->theme . 'front/checkout_login', $this->data);
	}

	public function shipping()
	{
		$this->load->view($this->theme . 'front/ck_shipping', $this->data);
	}


}
