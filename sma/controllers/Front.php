<?php defined('BASEPATH') or exit('No direct script access allowed');

class Front extends MY_Controller
{


    public function index()
    {
        
       $this->load->view($this->theme . 'front/index', $this->data);
    }


}
