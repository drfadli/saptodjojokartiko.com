<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends MY_Controller {


	public function signin()
	{
		$this->load->view($this->theme . 'front/signin', $this->data);
	}

	public function welcome()
	{
		$this->load->view($this->theme . 'front/welcome', $this->data);

	}

	public function recomendations()
	{
		$this->load->view($this->theme . 'front/recommendations', $this->data);
	}

	public function my_orders()
	{
		$this->load->view($this->theme . 'front/order_history', $this->data);
	}

	public function saved_items()
	{
		$this->load->view($this->theme . 'front/saved_items', $this->data);
	}

	public function setting()
	{
		$this->load->view($this->theme . 'front/setting', $this->data);
	}

	public function address_book()
	{
		$this->load->view($this->theme . 'front/address_book', $this->data);
	}

	public function wallet()
	{
		$this->load->view($this->theme . 'front/wallet', $this->data);
	}


}
