<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends MY_Controller {


	public function index()
    {
        
       $this->load->view($this->theme . 'front/news', $this->data);
    }

    public function detail()
    {
        
       $this->load->view($this->theme . 'front/detail_news', $this->data);
    }


}
