<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Module: Sales
 * Language: English
 * 
 * Last edited:
 * 30th April 2015
 *
 * Package:
 * Stock Manage Advance v3.0
 * 
 * You can translate this file to your language. 
 * For instruction on new language setup, please visit the documentations. 
 * You also can share your language files by emailing to saleem@tecdiary.com 
 * Thank you 
 */
$lang['add_user']                                   = 	"	إضافة مستخدم	"	;
$lang['notify_user_by_email']                       = 	"	إبلاغ المستخدمين عن طريق البريد الإلكتروني	"	;
$lang['group']                                      = 	"	مجموعة	"	;
$lang['edit_user']                                  = 	"	تحرير العضو	"	;
$lang['delete_user']                                = 	"	حذف العضو	"	;
$lang['user_added']                                 = 	"	تمت إضافة المستخدم بنجاح	"	;
$lang['user_updated']                               = 	"	تحديث المستخدم بنجاح	"	;
$lang['users_deleted']                              = 	"	حذف المستخدمين بنجاح	"	;
$lang['alert_x_user']                               = 	"	أنت ذاهب إلى إزالة هذا المستخدم بشكل دائم. اضغط موافق للمتابعة وإلغاء للعودة للخلف	"	;
$lang['login_email']                                = 	"	الدخول البريد الإلكتروني	"	;
$lang['edit_profile']                               = 	"	تحرير الملف الشخصي	"	;
$lang['website']                                    = 	"	الموقع	"	;
$lang['if_you_need_to_rest_password_for_user']      = 	"	إذا كنت بحاجة إلى إعادة تعيين كلمة المرور لهذا المستخدم.	"	;
$lang['user_options']                               = 	"	خيارات المستخدم	"	;
$lang['old_password']                               = 	"	كلمة السر القديمة	"	;
$lang['new_password']                               = 	"	كلمة السر الجديدة	"	;
$lang['change_avatar']                              = 	"	تغيير الصورة الرمزية	"	;
$lang['update_avatar']                              = 	"	تحديث الرمزية	"	;
$lang['avatar']                                     = 	"	الصورة الرمزية	"	;
$lang['avatar_deleted']                             = 	"	الصورة الرمزية تحديث بنجاح	"	;
$lang['captcha_wrong']                              = 	"	كلمة التحقق من الخطأ أو منتهية الصلاحية. يرجى المحاولة مرة أخرى	"	;
$lang['captcha']                                    = 	"	كلمة التحقق	"	;
$lang['site_is_offline_plz_try_later']              = 	"	الموقع حاليا. يرجى زيارتنا مرة أخرى في غضون أيام قليلة.	"	;
$lang['type_captcha']                               = 	"	اكتب كلمة التحقق	"	;
$lang['we_are_sorry_as_this_sction_is_still_under_development'] = 	"	ونحن نأسف لأن هذا الباب لا يزال قيد التطوير، ونحن نحاول أن نجعل في اسرع وقت ممكن. المريض الخاص بك هو في غاية الامتنان.	"	;
$lang['confirm']                                    = 	"	التأكيد	"	;
$lang['error_csrf']                                 = 	"	تزوير الطلب عبر المواقع الكشف أو انتهت صلاحية الرمز المميز CSRF. يرجى المحاولة مرة أخرى.	"	;
$lang['avatar_updated']                             = 	"	الصورة الرمزية تحديث بنجاح	"	;
$lang['registration_is_disabled']                   = 	"	التسجيل مغلق حساب.	"	;
$lang['login_to_your_account']                      = 	"	يرجى تسجيل الدخول إلى حسابك.	"	;
$lang['pw']                                         = 	"	كلمة المرور	"	;
$lang['remember_me']                                = 	"	تذكرني	"	;
$lang['forgot_your_password']                       = 	"	نسيت كلمة المرور؟	"	;
$lang['dont_worry']                                 = 	"	لا تقلق!	"	;
$lang['click_here']                                 = 	"	انقر هنا	"	;
$lang['to_rest']                                    = 	"	لإعادة تعيين	"	;
$lang['forgot_password']                            = 	"	نسيت كلمة المرور	"	;
$lang['login_successful']                           = 	"	قمت بتسجيل الدخول بنجاح في.	"	;
$lang['back']                                       = 	"	ظهر	"	;
$lang['dont_have_account']                          = 	"	لم يكن لديك حساب؟	"	;
$lang['no_worry']                                   = 	"	لا تقلق!	"	;
$lang['to_register']                                = 	"	للتسجيل	"	;
$lang['register_account_heading']                   = 	"	يرجى ملء النموذج أدناه لتسجيل حساب	"	;
$lang['register_now']                               = 	"	سجل الآن	"	;
$lang['no_user_selected']                           = 	"	أي مستخدم اختيار. الرجاء تحديد مستخدم واحد على الأقل.	"	;
$lang['delete_users']                               = 	"	حذف المستخدمين	"	;
$lang['delete_avatar']                              = 	"	حذف الصورة	"	;
$lang['deactivate_heading']                         = 	"	هل أنت متأكد من تعطيل المستخدم؟	"	;
$lang['deactivate']                                 = 	"	عطل	"	;
$lang['pasword_hint']                               = 	"	1 ما لا يقل رأس المال، و1 صغيرة (1)، وعدد أكثر من 8 أحرف	"	;
$lang['pw_not_same']                                = 	"	كلمة المرور وتأكيد كلمة السر ليست هي نفسها	"	;
$lang['reset_password']                             = 	"	إعادة تعيين كلمة المرور	"	;
$lang['reset_password_link_alt']                    = 	"	يمكنك لصق هذا الرمز أدناه في عنوان موقعك إذا كان الرابط أعلاه لا يعمل	"	;
$lang['email_forgotten_password_subject']           = 	"	إعادة تعيين كلمة المرور تفاصيل	"	;
$lang['reset_password_email']                       = 	"	إعادة تعيين كلمة المرور٪ الصورة	"	;
$lang['back_to_login']                              = 	"	العودة إلى تسجيل الدخول	"	;
$lang['forgot_password_unsuccessful']               = 	"	فشل إعادة تعيين كلمة المرور	"	;
$lang['forgot_password_successful']                 = 	"	إعادة تعيين كلمة المرور بنجاح، يرجى استخدام كلمة سر جديدة للدخول	"	;
$lang['password_change_unsuccessful']               = 	"	فشل تغيير كلمة المرور	"	;
$lang['password_change_successful']                 = 	"	تغيير كلمة المرور بنجاح	"	;
$lang['forgot_password_email_not_found']            = 	"	عنوان البريد الإلكتروني دخول لا ينتمي إلى أي حساب.	"	;
$lang['login_unsuccessful']                         = 	"	تسجيل الدخول فشل، يرجى المحاولة مرة أخرى	"	;
$lang['email_forgot_password_link']                 = 	"	إعادة تعيين كلمة المرور لينك	"	;
$lang['reset_password_heading']                     = 	"	إعادة تعيين كلمة المرور	"	;
$lang['reset_password_new_password_label']          = 	"	كلمة السر الجديدة	"	;
$lang['reset_password_new_password_confirm_label']  = 	"	تأكيد كلمة المرور الجديدة	"	;
$lang['register']                                   = 	"	التسجيل	"	;
$lang['reset_password_submit_btn']                  = 	"	إعادة تعيين كلمة المرور	"	;
$lang['error_csrf']                                 =	"	 لم هذا المنصب شكل لا يمر الشيكات أمننا.	"	;
$lang['account_creation_successful']                = 	"	تم إنشاء الحساب بنجاح	"	;
$lang['old_password_wrong']                         = 	"	الرجاء كتابة كلمة المرور القديمة الصحيحة	"	;
$lang['sending_email_failed']                       = 	"	غير قادر على إرسال البريد الإلكتروني، يرجى التحقق من إعدادات النظام.	"	;
$lang['deactivate_successful']                      = 	"	إلغاء تنشيط المستخدم بنجاح	"	;
$lang['activate_successful']                        = 	"	تنشيط المستخدم بنجاح	"	;
$lang['login_timeout']                              = 	"	لديك 3 محاولات الدخول الفاشلة. يرجى المحاولة بعد 10 دقيقة	"	;

// $lang['add_user']                                   = "Add User";
// $lang['notify_user_by_email']                       = "Notify User by Email";
// $lang['group']                                      = "Group";
// $lang['edit_user']                                  = "Edit User";
// $lang['delete_user']                                = "Delete User";
// $lang['user_added']                                 = "User successfully added";
// $lang['user_updated']                               = "User successfully updated";
// $lang['users_deleted']                              = "Users successfully deleted";
// $lang['alert_x_user']                               = "You are going to remove this user permanently. Press OK to proceed and Cancel to Go Back";
// $lang['login_email']                                = "Login Email";
// $lang['edit_profile']                               = "Edit Profile";
// $lang['website']                                    = "Website";
// $lang['if_you_need_to_rest_password_for_user']      = "If you need to reset the password for this user.";
// $lang['user_options']                               = "User options";
// $lang['old_password']                               = "Old Password";
// $lang['new_password']                               = "New Password";
// $lang['change_avatar']                              = "Change Avatar";
// $lang['update_avatar']                              = "Update Avatar";
// $lang['avatar']                                     = "Avatar";
// $lang['avatar_deleted']                             = "Avatar successfully updated";
// $lang['captcha_wrong']                              = "Captcha is wrong or expired. Please try again";
// $lang['captcha']                                    = "Captcha";
// $lang['site_is_offline_plz_try_later']              = "Site is offline. Please visit us again in few days.";
// $lang['type_captcha']                               = "Type Captcha";
// $lang['we_are_sorry_as_this_sction_is_still_under_development'] = "We are sorry as this section is still under development and we are try to make it asap. Your patient is highly appreciated.";
// $lang['confirm']                                    = "Confirmation";
// $lang['error_csrf']                                 = "Cross-site request forgery detected or the csrf token is expired. Please try again.";
// $lang['avatar_updated']                             = "Avatar successfully updated";
// $lang['registration_is_disabled']                   = "Account Registration is closed.";
// $lang['login_to_your_account']                      = "Please login to your account.";
// $lang['pw']                                         = "Password";
// $lang['remember_me']                                = "Remember me";
// $lang['forgot_your_password']                       = "Forgot your password?";
// $lang['dont_worry']                                 = "Don't worry!";
// $lang['click_here']                                 = "click here";
// $lang['to_rest']                                    = "to reset";
// $lang['forgot_password']                            = "Forgot Password";
// $lang['login_successful']                           = "You are successfully logged in.";
// $lang['back']                                       = "Back";
// $lang['dont_have_account']                          = "Don't have account?";
// $lang['no_worry']                                   = "No worries!";
// $lang['to_register']                                = "to register";
// $lang['register_account_heading']                   = "Please fill in the form below to register account";
// $lang['register_now']                               = "Register Now";
// $lang['no_user_selected']                           = "No user selected. Please select at least one user.";
// $lang['delete_users']                               = "Delete Users";
// $lang['delete_avatar']                              = "Delete Avatar";
// $lang['deactivate_heading']                         = "Are you sure to deactivate the user?";
// $lang['deactivate']                                 = "Deactivate";
// $lang['pasword_hint']                               = "At least 1 capital, 1 lowercase, 1 number and more than 8 characters long";
// $lang['pw_not_same']                                = "The password and confirm password are not the same";
// $lang['reset_password']                             = "Reset Password";
// $lang['reset_password_link_alt']                    = "You can paste this code below in your url if the above link not working";
// $lang['email_forgotten_password_subject']           = "Reset Password Details";
// $lang['reset_password_email']                       = "Reset Password %s";
// $lang['back_to_login']                              = "Back to login";
// $lang['forgot_password_unsuccessful']               = "Reset password failed";
// $lang['forgot_password_successful']                 = "Password successfully reset, please use new password to login";
// $lang['password_change_unsuccessful']               = "Password change failed";
// $lang['password_change_successful']                 = "Password successfully changed";
// $lang['forgot_password_email_not_found']            = "The email address enter does not belong to any account.";
// $lang['login_unsuccessful']                         = "Login Failed, Please try again";
// $lang['email_forgot_password_link']                 = "Reset Password Link";
// $lang['reset_password_heading']                     = "Reset Password";
// $lang['reset_password_new_password_label']          = "New Password";
// $lang['reset_password_new_password_confirm_label']  = "Confirm New Password";
// $lang['register']                                   = "Register";
// $lang['reset_password_submit_btn']                  = "Reset Password";
// $lang['error_csrf']                                 = 'This form post did not pass our security checks.';
// $lang['account_creation_successful']                = "Account successfully created";
// $lang['old_password_wrong']                         = "Please type correct old password";
// $lang['sending_email_failed']                       = "Unable to send email, please check you system settings.";
// $lang['deactivate_successful']                      = "User successfully deactivated";
// $lang['activate_successful']                        = "User successfully activated";
// $lang['login_timeout']                              = "You have 3 failed login attempts. Please try after 10 minutes";