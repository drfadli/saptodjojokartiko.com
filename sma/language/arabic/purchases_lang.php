<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Language: English
 * Module: Purchases
 *
 * Last edited:
 * 30th April 2015
 *
 * Package:
 * Stock Manage Advance v3.0
 *
 * You can translate this file to your language.
 * For instruction on new language setup, please visit the documentations.
 * You also can share your language files by emailing to saleem@tecdiary.com
 * Thank you
 */

$lang['add_purchase']                       = 	"	إضافة شراء	"	;
$lang['edit_purchase']                      = 	"	تحرير شراء	"	;
$lang['delete_purchase']                    = 	"	حذف شراء	"	;
$lang['delete_purchases']                   = 	"	حذف شراؤها	"	;
$lang['purchase_added']                     = 	"	شراء أضاف بنجاح	"	;
$lang['purchase_updated']                   = 	"	شراء تحديثها بنجاح	"	;
$lang['purchase_deleted']                   = 	"	شراء حذف بنجاح	"	;
$lang['purchases_deleted']                  = 	"	حذف مشتريات بنجاح	"	;
$lang['ref_no']                             = 	"	إشارة لا	"	;
$lang['purchase_details']                   = 	"	تفاصيل شراء	"	;
$lang['email_purchase']                     = 	"	البريد الإلكتروني شراء	"	;
$lang['purchase_quantity']                  = 	"	شراء الكمية	"	;
$lang['please_select_warehouse']            = 	"	الرجاء تحديد مستودع	"	;
$lang['purchase_by_csv']                    = 	"	إضافة شراء من قبل CSV	"	;
$lang['received']                           = 	"	تلقى	"	;
$lang['more_options']                       = 	"	المزيد من الخيارات	"	;
$lang['add_standard_product']               = 	"	إضافة معيار المنتج	"	;
$lang['product_code_is_required']           = 	"	مطلوب رمز المنتج	"	;
$lang['product_name_is_required']           = 	"	مطلوب اسم المنتج	"	;
$lang['product_category_is_required']       = 	"	مطلوب فئة المنتج	"	;
$lang['product_unit_is_required']           = 	"	مطلوب وحدة المنتج	"	;
$lang['product_cost_is_required']           = 	"	مطلوب تكلفة المنتج	"	;
$lang['product_price_is_required']          = 	"	مطلوب سعر المنتج	"	;
$lang['ordered']                            = 	"	أمر	"	;
$lang['tax_rate_name']                      = 	"	اسم الضرائب تقييم	"	;
$lang['first_3_are_required_other_optional'] = 	"	مطلوبة على <strong> ثلاثة أعمدة الأول والبعض الآخر اختياري. </ قوي>	"	;
$lang['no_purchase_selected']               = 	"	أي شراء المحدد. يرجى اختيار شراء واحدة على الأقل.	"	;
$lang['view_payments']                      = 	"	عرض المدفوعات	"	;
$lang['add_payment']                        = 	"	إضافة الدفع	"	;
$lang['payment_reference_no']               = 	"	دفع مرجع رقم	"	;
$lang['edit_payment']                       = 	"	تحرير الدفع	"	;
$lang['delete_payment']                     = 	"	حذف الدفع	"	;
$lang['delete_payments']                    = 	"	حذف المدفوعات	"	;
$lang['payment_added']                      = 	"	وأضاف الدفع بنجاح	"	;
$lang['payment_updated']                    = 	"	الدفع بنجاح تحديث	"	;
$lang['payment_deleted']                    = 	"	حذف الدفع بنجاح	"	;
$lang['payments_deleted']                   = 	"	حذف المدفوعات بنجاح	"	;
$lang['paid_by']                            = 	"	يتحملها	"	;
$lang['payment_reference']                  = 	"	دفع المرجعي	"	;
$lang['view_purchase_details']              = 	"	عرض تفاصيل شراء	"	;
$lang['purchase_no']                        = 	"	عدد شراء	"	;
$lang['stamp_sign']                         = 	"	ختم وتوقيع	"	;
$lang['balance']                            = 	"	توازن	"	;
$lang['product_option']                     = 	"	المنتج الخيار	"	;
$lang['payment_sent']                       = 	"	دفع المرسلة	"	;
$lang['payment_note']                       = 	"	دفع ملاحظة	"	;
$lang['payment_received']                   = 	"	تلقى الدفع	"	;
$lang['purchase_status']                    = 	"	حالة الشراء	"	;
$lang['purchase_x_edited_older_than_3_months'] = 	"	شراء لا يمكن أن تعدل لأن هذا الشراء هو أقدم من 3 أشهر. يمكنك تعديل المشتريات في غضون 3 أشهر.	"	;
$lang['pr_not_found']                       = 	"	لم يتم العثور على المنتج	"	;
$lang['line_no']                            = 	"	عدد خط	"	;
$lang['expense']                            = 	"	نفقة	"	;
$lang['edit_expense']                       = 	"	تحرير المصاريف	"	;
$lang['delete_expense']                     = 	"	حذف المصاريف	"	;
$lang['delete_expenses']                    = 	"	حذف المصروفات	"	;
$lang['expense_added']                      = 	"	وأضاف نفقة بنجاح	"	;
$lang['expense_updated']                    = 	"	حساب تحديثها بنجاح	"	;
$lang['expense_deleted']                    = 	"	حذف حساب بنجاح	"	;
$lang['reference']                          = 	"	إشارة	"	;
$lang['expenses_deleted']                   = 	"	حذف نفقات بنجاح	"	;
$lang['expense_note']                       = 	"	حساب ملاحظة	"	;
$lang['no_expense_selected']                = 	"	لا حساب المحدد. الرجاء تحديد حساب واحد على الأقل.	"	;
$lang['please_select_supplier']             = 	"	الرجاء تحديد المورد	"	;
$lang['unit_cost']                          = 	"	تكلفة الوحدة	"	;
$lang['product_expiry_date_issue']          = 	"	المنتج تاريخ انتهاء الصلاحية ديه قضية	"	;



// $lang['add_purchase']                       = "Add Purchase";
// $lang['edit_purchase']                      = "Edit Purchase";
// $lang['delete_purchase']                    = "Delete Purchase";
// $lang['delete_purchases']                   = "Delete Purchases";
// $lang['purchase_added']                     = "Purchase successfully added";
// $lang['purchase_updated']                   = "Purchase successfully updated";
// $lang['purchase_deleted']                   = "Purchase successfully deleted";
// $lang['purchases_deleted']                  = "Purchases successfully deleted";
// $lang['ref_no']                             = "Reference No";
// $lang['purchase_details']                   = "Purchase Details";
// $lang['email_purchase']                     = "Email Purchase";
// $lang['purchase_quantity']                  = "Purchase Quantity";
// $lang['please_select_warehouse']            = "Please select warehouse";
// $lang['purchase_by_csv']                    = "Add Purchase by CSV";
// $lang['received']                           = "Received";
// $lang['more_options']                       = "More Options";
// $lang['add_standard_product']               = "Add Standard Product";
// $lang['product_code_is_required']           = "Product Code is required";
// $lang['product_name_is_required']           = "Product Name is required";
// $lang['product_category_is_required']       = "Product Category is required";
// $lang['product_unit_is_required']           = "Product Unit is required";
// $lang['product_cost_is_required']           = "Product Cost is required";
// $lang['product_price_is_required']          = "Product Price is required";
// $lang['ordered']                            = "Ordered";
// $lang['tax_rate_name']                      = "Tax Rate Name";
// $lang['first_3_are_required_other_optional'] = "<strong>First three columns are required and others are optional.</strong>";
// $lang['no_purchase_selected']               = "No purchase selected. Please select at least one purchase.";
// $lang['view_payments']                      = "View Payments";
// $lang['add_payment']                        = "Add Payment";
// $lang['payment_reference_no']               = "Payment Reference No";
// $lang['edit_payment']                       = "Edit Payment";
// $lang['delete_payment']                     = "Delete Payment";
// $lang['delete_payments']                    = "Delete Payments";
// $lang['payment_added']                      = "Payment successfully added";
// $lang['payment_updated']                    = "Payment successfully updated";
// $lang['payment_deleted']                    = "Payment successfully deleted";
// $lang['payments_deleted']                   = "Payments successfully deleted";
// $lang['paid_by']                            = "Paid by";
// $lang['payment_reference']                  = "Payment Reference";
// $lang['view_purchase_details']              = "View Purchase Details";
// $lang['purchase_no']                        = "Purchase Number";
// $lang['stamp_sign']                         = "Stamp & Signature";
// $lang['balance']                            = "Balance";
// $lang['product_option']                     = "Product Option";
// $lang['payment_sent']                       = "Payment Sent";
// $lang['payment_note']                       = "Payment Note";
// $lang['payment_received']                   = "Payment Received";
// $lang['purchase_status']                    = "Purchase Status";
// $lang['purchase_x_edited_older_than_3_months'] = "Purchase can't be edited as this purchase is older than 3 months. You can edit purchases within 3 months.";
// $lang['pr_not_found']                       = "No product found ";
// $lang['line_no']                            = "Line Number";
// $lang['expense']                            = "Expense";
// $lang['edit_expense']                       = "Edit Expense";
// $lang['delete_expense']                     = "Delete Expense";
// $lang['delete_expenses']                    = "Delete Expenses";
// $lang['expense_added']                      = "Expense successfully added";
// $lang['expense_updated']                    = "Expense successfully updated";
// $lang['expense_deleted']                    = "Expense successfully deleted";
// $lang['reference']                          = "Reference";
// $lang['expenses_deleted']                   = "Expenses successfully deleted";
// $lang['expense_note']                       = "Expense Note";
// $lang['no_expense_selected']                = "No expense selected. Please select at least one expense.";
// $lang['please_select_supplier']             = "Please select supplier";
// $lang['unit_cost']                          = "Unit Cost";
// $lang['product_expiry_date_issue']          = "Product expiry date has issue";
