<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Language: English
 * Module: Quotations
 * 
 * Last edited:
 * 30th April 2015
 *
 * Package:
 * Stock Manage Advance v3.0
 * 
 * You can translate this file to your language. 
 * For instruction on new language setup, please visit the documentations. 
 * You also can share your language files by emailing to saleem@tecdiary.com 
 * Thank you 
 */

$lang['add_quote']                      = 	"	إضافة الاقتباس	"	;
$lang['edit_quote']                     = 	"	تحرير الاقتباس	"	;
$lang['delete_quote']                   = 	"	حذف الاقتباس	"	;
$lang['delete_quotes']                  = 	"	حذف الاقتباسات	"	;
$lang['quote_added']                    = 	"	وأضاف اقتباس بنجاح	"	;
$lang['quote_updated']                  = 	"	اقتباس تحديث بنجاح	"	;
$lang['quote_deleted']                  = 	"	حذف الاقتباس بنجاح	"	;
$lang['quotes_deleted']                 = 	"	حذف الاقتباسات بنجاح	"	;
$lang['quote_details']                  = 	"	تفاصيل الاقتباس	"	;
$lang['email_quote']                    = 	"	البريد الإلكتروني الاقتباس	"	;
$lang['view_quote_details']             = 	"	عرض تفاصيل الاقتباس	"	;
$lang['quote_no']                       = 	"	اقتباس لا	"	;
$lang['send_email']                     = 	"	إرسال البريد الإلكتروني	"	;
$lang['quote_items']                    = 	"	اقتباس الأصناف	"	;
$lang['no_quote_selected']              = 	"	لا اقتباس المحدد. الرجاء تحديد اقتباس واحد على الأقل.	"	;
$lang['stamp_sign']                     = 	"	القضاء و. توقيع	"	;
$lang['create_sale']                    = 	"	إنشاء بيع	"	;
$lang['create_purchase']                = 	"	قفص شراء	"	;


// $lang['add_quote']                      = "Add Quotation";
// $lang['edit_quote']                     = "Edit Quotation";
// $lang['delete_quote']                   = "Delete Quotation";
// $lang['delete_quotes']                  = "Delete Quotations";
// $lang['quote_added']                    = "Quotation successfully added";
// $lang['quote_updated']                  = "Quotation successfully updated";
// $lang['quote_deleted']                  = "Quotation successfully deleted";
// $lang['quotes_deleted']                 = "Quotations successfully deleted";
// $lang['quote_details']                  = "Quotation Details";
// $lang['email_quote']                    = "Email Quotation";
// $lang['view_quote_details']             = "View Quotation Details";
// $lang['quote_no']                       = "Quotation No";
// $lang['send_email']                     = "Send Email";
// $lang['quote_items']                    = "Quotation Items";
// $lang['no_quote_selected']              = "No quotation selected. Please select at least one quotation.";
// $lang['stamp_sign']                     = "Stamp &amp; Signature";
// $lang['create_sale']                    = "Create Sale";
// $lang['create_purchase']                = "Create Purcahse";