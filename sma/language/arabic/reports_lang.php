<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Language: English
 * Module: Reports
 *
 * Last edited:
 * 30th April 2015
 *
 * Package:
 * Stock Manage Advance v3.0
 *
 * You can translate this file to your language.
 * For instruction on new language setup, please visit the documentations.
 * You also can share your language files by emailing to saleem@tecdiary.com
 * Thank you
 */
$lang['profit_estimate']            = 	"	الربح تقدير	"	;
$lang['warehouse_stock_heading']    = 	"	مستودع الأوراق المالية القيمة التي التكلفة والأسعار. الرجاء تحديد مستودع على الحق في الحصول على قيمة التخزين المحدد.	"	;
$lang['alert_quantity']             = 	"	تنبيه الكمية	"	;
$lang['customize_report']           = 	"	يرجى تخصيص التقرير أدناه	"	;
$lang['start_date']                 = 	"	بدء التسجيل	"	;
$lang['end_date']                   = 	"	تاريخ الانتهاء	"	;
$lang['purchased_amount']           = 	"	المبلغ شراء	"	;
$lang['sold_amount']                = 	"	المبلغ المباع	"	;
$lang['profit_loss']                = 	"	الأرباح و / أو الخسارة	"	;
$lang['daily_sales_report']         = 	"	تقرير المبيعات اليومي	"	;
$lang['reports_calendar_text']      = 	"	يمكنك تغيير الشهر، بالضغط >> (التالي) أو << (السابق)	"	;
$lang['monthly_sales_report']       = 	"	تقرير المبيعات الشهرية	"	;
$lang['product_qty']                = 	"	المنتج (الكمية)	"	;
$lang['payment_ref']                = 	"	دفع المرجعي	"	;
$lang['sale_ref']                   = 	"	بيع المرجعي	"	;
$lang['purchase_ref']               = 	"	شراء المرجعي	"	;
$lang['paid_by']                    = 	"	يتحملها	"	;
$lang['view_report']                = 	"	عرض التقرير	"	;
$lang['sales_amount']               = 	"	المبلغ المبيعات	"	;
$lang['total_paid']                 = 	"	إجمالي المدفوع	"	;
$lang['due_amount']                 = 	"	المبلغ المستحق	"	;
$lang['total_sales']                = 	"	مجموع المبيعات	"	;
$lang['total_quotes']               = 	"	مجموع الاقتباسات	"	;
$lang['customer_sales_report']      = 	"	تقرير مبيعات العملاء	"	;
$lang['customer_payments_report']   = 	"	المدفوعات العملاء تقرير	"	;
$lang['purchases_amount']           = 	"	مشتريات المبلغ	"	;
$lang['total_purchases']            = 	"	مجموع مشتريات	"	;
$lang['view_report_customer']       = 	"	الرجاء الضغط التقرير بغية التحقق من التقرير العملاء.	"	;
$lang['view_report_supplier']       = 	"	الرجاء الضغط التقرير بغية التحقق من التقرير المورد.	"	;
$lang['view_report_staff']          = 	"	الرجاء الضغط التقرير بغية التحقق من تقرير الخبراء.	"	;
$lang['staff_purchases_report']     = 	"	تقرير شراء الموظفين	"	;
$lang['staff_sales_report']         = 	"	تقرير مبيعات الموظفين	"	;
$lang['staff_payments_report']      = 	"	المدفوعات الموظفين تقرير	"	;
$lang['group']                      = 	"	مجموعة	"	;
$lang['staff_daily_sales']          = 	"	المبيعات اليومية	"	;
$lang['staff_monthly_sales']        = 	"	المبيعات الشهرية	"	;
$lang['staff_logins_report']        = 	"	تقرير محدودة الموظفين	"	;
$lang['add_customer']               = 	"	زبون	"	;
$lang['show_form']                  = 	"	مشاهدة نموذج	"	;
$lang['hide_form']                  = 	"	إخفاء النموذج	"	;
$lang['view_pl_report']             = 	"	يرجى الاطلاع على الربح و / أو فقدان التقرير ويمكنك تحديد نطاق التاريخ لتخصيص التقرير.	"	;
$lang['payments_sent']              = 	"	الدفعات المرسلة	"	;
$lang['payments_received']          = 	"	المدفوعات المتلقى	"	;
$lang['cheque']                     = 	"	شيك	"	;
$lang['cash']                       = 	"	النقد	"	;
$lang['CC']                         = 	"	بطاقة إئتمان	"	;
$lang['paypal_pro']                 = 	"	باي بال برو	"	;
$lang['stripe']                     = 	"	شريط	"	;
$lang['cc_slips']                   = 	"	CC الزلات	"	;
$lang['total_cash']                 = 	"	المجموع نقد	"	;
$lang['open_time']                  = 	"	مفتوحة الوقت	"	;
$lang['close_time']                 = 	"	انهيار الوقت	"	;
$lang['cash_in_hand']               = 	"	النقد في الصندوق	"	;
$lang['Cheques']                    = 	"	الشيكات	"	;
$lang['save_image']                 = 	"	حفظ ك صورة	"	;
$lang['total_quantity']             = 	"	مجموع الكمية	"	;
$lang['total_items']                = 	"	مجموع الوحدات	"	;
$lang['download_xls']               = 	"	تحميل كما XLS	"	;
$lang['transfers_report']           = 	"	نقل تقرير	"	;
$lang['transfer_no']                = 	"	عدد نقل	"	;
$lang['sales_return_report']        = 	"	مبيعات رجوع تقرير	"	;
$lang['payments_returned']          = 	"	المدفوعات إرجاعها	"	;
$lang['category_code']              = 	"	كود فئة	"	;
$lang['category_name']              = 	"	اسم التصنيف	"	;
$lang['total_returns']              = 	"	مجموع العائدات	"	;


// $lang['profit_estimate']            = "Profit Estimate";
// $lang['warehouse_stock_heading']    = "Warehouse Stock Value by Cost and Price. Please select the warehouse on right to get the value for selected warehouse.";
// $lang['alert_quantity']             = "Alert Quantity";
// $lang['customize_report']           = "Please customize the report below";
// $lang['start_date']                 = "Start Date";
// $lang['end_date']                   = "End Date";
// $lang['purchased_amount']           = "Purchased Amount";
// $lang['sold_amount']                = "Sold Amount";
// $lang['profit_loss']                = "Profit and/or Loss";
// $lang['daily_sales_report']         = "Daily Sales Report";
// $lang['reports_calendar_text']      = "You can change the month by clicking the >> (next) or << (previous)";
// $lang['monthly_sales_report']       = "Monthly Sales Report";
// $lang['product_qty']                = "Product (Qty)";
// $lang['payment_ref']                = "Payment Reference";
// $lang['sale_ref']                   = "Sale Reference";
// $lang['purchase_ref']               = "Purchase Reference";
// $lang['paid_by']                    = "Paid by";
// $lang['view_report']                = "View Report";
// $lang['sales_amount']               = "Sales Amount";
// $lang['total_paid']                 = "Total Paid";
// $lang['due_amount']                 = "Due Amount";
// $lang['total_sales']                = "Total Sales";
// $lang['total_quotes']               = "Total Quotations";
// $lang['customer_sales_report']      = "Customer Sales Report";
// $lang['customer_payments_report']   = "Customer Payments Report";
// $lang['purchases_amount']           = "Purchases Amount";
// $lang['total_purchases']            = "Total Purchases";
// $lang['view_report_customer']       = "Please click view report to check the customer report.";
// $lang['view_report_supplier']       = "Please click view report to check the supplier report.";
// $lang['view_report_staff']          = "Please click view report to check the staff report.";
// $lang['staff_purchases_report']     = "Staff Purchase Report";
// $lang['staff_sales_report']         = "Staff Sales Report";
// $lang['staff_payments_report']      = "Staff Payments Report";
// $lang['group']                      = "Group";
// $lang['staff_daily_sales']          = "Daily Sales";
// $lang['staff_monthly_sales']        = "Monthly Sales";
// $lang['staff_logins_report']        = "Staff Logins Report";
// $lang['add_customer']               = "Customer";
// $lang['show_form']                  = "Show Form";
// $lang['hide_form']                  = "Hide Form";
// $lang['view_pl_report']             = "Please view the Profit and/or Loss report and you can select the date range to customized the report.";
// $lang['payments_sent']              = "Payments Sent";
// $lang['payments_received']          = "Payments Received";
// $lang['cheque']                     = "Cheque";
// $lang['cash']                       = "Cash";
// $lang['CC']                         = "Credit Card";
// $lang['paypal_pro']                 = "Paypal Pro";
// $lang['stripe']                     = "Stripe";
// $lang['cc_slips']                   = "CC Slips";
// $lang['total_cash']                 = "Total Cash";
// $lang['open_time']                  = "Open Time";
// $lang['close_time']                 = "Close Time";
// $lang['cash_in_hand']               = "Cash in hand";
// $lang['Cheques']                    = "Cheques";
// $lang['save_image']                 = "Save as Image";
// $lang['total_quantity']             = "Total Quantity";
// $lang['total_items']                = "Total Items";
// $lang['download_xls']               = "Download as XLS";
// $lang['transfers_report']           = "Transfers Report";
// $lang['transfer_no']                = "Transfer Number";
// $lang['sales_return_report']        = "Sales Return Report";
// $lang['payments_returned']          = "Payments Returned";
// $lang['category_code']              = "Category Code";
// $lang['category_name']              = "Category Name";
// $lang['total_returns']              = "Total Returns";
