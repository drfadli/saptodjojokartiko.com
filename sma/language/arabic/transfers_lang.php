<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Language: English
 * Module: Transfers
 * 
 * Last edited:
 * 30th April 2015
 *
 * Package:
 * Stock Manage Advance v3.0
 * 
 * You can translate this file to your language. 
 * For instruction on new language setup, please visit the documentations. 
 * You alseo can share your language files by emailing to saleem@tecdiary.com 
 * Thank you 
 */

$lang['add_transfer']                                   = 	"	إضافة نقل	"	;
$lang['edit_transfer']                                  = 	"	تحرير النقل	"	;
$lang['delete_transfer']                                = 	"	حذف نقل	"	;
$lang['delete_transfers']                               = 	"	حذف التحويلات	"	;
$lang['transfer_added']                                 = 	"	نقل وأضاف بنجاح	"	;
$lang['transfer_updated']                               = 	"	نقل تحديثها بنجاح	"	;
$lang['transfer_deleted']                               = 	"	نقل حذف بنجاح	"	;
$lang['transfers_deleted']                              = 	"	حذف نقل بنجاح	"	;
$lang['import_by_csv']                                  = 	"	إضافة التحويلات التي كتبها CSV	"	;
$lang['ref_no']                                         = 	"	إشارة لا	"	;
$lang['transfer_details']                               = 	"	تفاصيل نقل	"	;
$lang['email_transfer']                                 = 	"	نقل البريد الإلكتروني	"	;
$lang['transfer_quantity']                              = 	"	نقل الكمية	"	;
$lang['please_select_warehouse']                        = 	"	الرجاء تحديد مستودع	"	;
$lang['please_select_different_warehouse']              = 	"	الرجاء تحديد مستودع مختلفة	"	;
$lang['to_warehouse']                                   = 	"	إلى مستودع	"	;
$lang['from_warehouse']                                 = 	"	من مستودع	"	;
$lang['edit_transfer_quantity']                         = 	"	تحرير نقل الكمية	"	;
$lang['can_not_change_status_of_completed_transfer']    = 	"	لا يمكنك تغيير وضع نقل الانتهاء	"	;
$lang['transfer_by_csv']                                = 	"	إضافة نقل من قبل CSV	"	;
$lang['no_transfer_selected']                           = 	"	لا نقل المحدد. الرجاء تحديد نقل واحد على الأقل.	"	;
$lang['stamp_sign']                                     = 	"	القضاء و. توقيع	"	;
$lang['received_by']                                    = 	"	استقبل من قبل	"	;
$lang['users']                                          = 	"	المستخدمين	"	;
$lang['transferring']                                   = 	"	نقل	"	;
$lang['first_2_are_required_other_optional']            = 	"	<strong>مطلوبة على  عمودين الأول والبعض الآخر اختياري. </strong>	"	;
$lang['pr_not_found']                                   = 	"	لم يتم العثور على المنتج	"	;
$lang['line_no']                                        = 	"	عدد خط	"	;


// $lang['add_transfer']                                   = "Add Transfer";
// $lang['edit_transfer']                                  = "Edit Transfer";
// $lang['delete_transfer']                                = "Delete Transfer";
// $lang['delete_transfers']                               = "Delete Transfers";
// $lang['transfer_added']                                 = "Transfer successfully added";
// $lang['transfer_updated']                               = "Transfer successfully updated";
// $lang['transfer_deleted']                               = "Transfer successfully deleted";
// $lang['transfers_deleted']                              = "Transfers successfully deleted";
// $lang['import_by_csv']                                  = "Add Transfers by CSV";
// $lang['ref_no']                                         = "Reference No";
// $lang['transfer_details']                               = "Transfer Details";
// $lang['email_transfer']                                 = "Email Transfer";
// $lang['transfer_quantity']                              = "Transfer Quantity";
// $lang['please_select_warehouse']                        = "Please select warehouse";
// $lang['please_select_different_warehouse']              = "Please select different warehouse";
// $lang['to_warehouse']                                   = "To Warehouse";
// $lang['from_warehouse']                                 = "From Warehouse";
// $lang['edit_transfer_quantity']                         = "Edit Transfer Quantity";
// $lang['can_not_change_status_of_completed_transfer']    = "You can't change the status of completed transfer";
// $lang['transfer_by_csv']                                = "Add Transfer by CSV";
// $lang['no_transfer_selected']                           = "No transfer selected. Please select at least one transfer.";
// $lang['stamp_sign']                                     = "Stamp &amp; Signature";
// $lang['received_by']                                    = "Received by";
// $lang['users']                                          = "Users";
// $lang['transferring']                                   = "Transferring";
// $lang['first_2_are_required_other_optional']            = "<strong>First two columns are required and others are optional.</strong>";
// $lang['pr_not_found']                                   = "No product found ";
// $lang['line_no']                                        = "Line Number";