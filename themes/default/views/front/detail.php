<!DOCTYPE html>
<html>
<head>
	<?php $this->load->view($this->theme . 'front/include/head'); ?>

	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/front/css/slider.css">

</head>
<body>
	<header>
		<?php $this->load->view($this->theme . 'front/include/header'); ?>
	</header>

	<div class="filter">
		<div class="container-fluid">
			<ul class="menu-filter menu-left">
				<li>
					<a href="">Store</a>
				</li>
				<li class="divider"></li>
				<li>
					<a href="">Collections</a>
				</li>
				<li class="divider"></li>
				<li>
					<a href="">Dresses</a>
				</li>
			</ul>
			<ul class="menu-filter menu-right">
				<li>
					<select>
						<option>Categori</option>
						<option>Categori</option>
					</select>
				</li>
				<li class="divider"></li>
				<li>
					<select>
						<option>Filters</option>
						<option>Filters</option>
					</select>
				</li>
				<li class="divider"></li>
				<li>
					<select>
						<option>Short By : <b>Newest</b></option>
						<option>Short By : <b>Newest</b></option>
					</select>
				</li>
			</ul>
		</div>
	</div>


	<div id="detail-product">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 col-md-6">
					<div id="carousel-vertical" class="carousel vertical slide">

						<div class="carousel-inner" role="listbox">

							<div class="item active">
								<div class="container">
									<div class="row">
										<div class="col-lg-6 col-md-6">
											<div class="csslider infinity" id="slider1">
												<input type="radio" name="slides" id="slides_1"/>
												<input type="radio" name="slides" checked="checked" id="slides_2"/>
												<input type="radio" name="slides" id="slides_3"/>
												<input type="radio" name="slides" id="slides_4"/>
												<input type="radio" name="slides" id="slides_5"/>
												<input type="radio" name="slides" id="slides_6"/>
												<ul>
													<li>
														<img src="<?=base_url()?>assets/front/img/img-product.png"/>
													</li>
													<li>
														<img src="<?=base_url()?>assets/front/img/img-product.png"/>
													</li>
													<li>
														<img src="<?=base_url()?>assets/front/img/img-product.png"/>
													</li>
													<li>
														<img src="<?=base_url()?>assets/front/img/img-product.png"/>
													</li>
													<li>
														<img src="<?=base_url()?>assets/front/img/img-product.png"/>
													</li>
													<li>
														<img src="<?=base_url()?>assets/front/img/img-product.png"/>
													</li>
												</ul>
												<div class="arrows">
													<label for="slides_1"></label>
													<label for="slides_2"></label>
													<label for="slides_3"></label>
													<label for="slides_4"></label>
													<label for="slides_5"></label>
													<label for="slides_6"></label>
													<label class="goto-first" for="slides_1"></label>
													<label class="goto-last" for="slides_6"></label>
												</div>
												<div class="navigation"> 
													<div>
														<label for="slides_1"></label>
														<label for="slides_2"></label>
														<label for="slides_3"></label>
														<label for="slides_4"></label>
														<label for="slides_5"></label>
														<label for="slides_6"></label>
													</div>
												</div>
											</div>
										</div>	
									</div>
								</div>
							</div>

							<div class="item">
								<div class="container">
									<div class="row">
										
										<div class="col-lg-6 col-md-6">
											<div class="desc-product">
												<p class="title-product-2">Product Description</p>
												<p>A short sleeve dress with a round neck made in stretch viscose jersey.
													The neckline is trimmed with pressed fabric in a torchon shape outlined
													by crystals. The bow continues to be a defining feature of the House
												placed here on the waist.</p>

												<ul class="list-desc">
													<li> • Black compact stretch viscose jersey</li>
													<li> • Pressed stretch viscose jersey torchon with crystal trim</li>
													<li> • Back zip closure</li>
													<li> • 75% viscose, 17% nylon and 8% elastane</li>
													<li> • Made in Italy</li>
													<li> • the model is 5’9" and is wearing a size small</li>
												</ul>
											</div>

											<div class="panel-desc">
												<div class="panel-group" id="accordion">
													<div class="panel panel-default">
														<div class="panel-heading">
															<h4 class="panel-title">
																<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#desc-panel-One">
																	Shipping Info
																</a>
															</h4>
														</div>
														<div id="desc-panel-One" class="panel-collapse collapse">
															<div class="panel-body">
																Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
															</div>
														</div>
													</div>
												</div>
											</div>

										</div>

									</div>
								</div>
							</div>
						</div>

						<div class="indicators-vertical">
							<ol class="carousel-indicators">
								<li data-target="#carousel-vertical" data-slide-to="0" class="active"></li>
								<li data-target="#carousel-vertical" data-slide-to="1"></li>
								<li data-target="#carousel-vertical" data-slide-to="2"></li>
							</ol>
						</div>

					</div>
				</div>

				<div class="col-lg-6 col-md-6">
					<div class="desc-product">
						<p class="title-product">Stretch Jersey Dress with Crystal Trim</p>
						<h4 class="price">$ 3,500</h4>
						<div class="style-code">
							<p>Style 501513 X9M65 1082</p>
						</div>
						<p class="color">Color</p>
						<div class="custom-radios">
							<div>
								<input type="radio" id="color-1" name="color" value="color-1" checked>
								<label for="color-1">
									<span>
										<img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/242518/check-icn.svg" alt="Checked Icon" />
									</span>
								</label>
							</div>

							<div>
								<input type="radio" id="color-2" name="color" value="color-2">
								<label for="color-2">
									<span>
										<img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/242518/check-icn.svg" alt="Checked Icon" />
									</span>
								</label>
							</div>

							<div>
								<input type="radio" id="color-3" name="color" value="color-3">
								<label for="color-3">
									<span>
										<img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/242518/check-icn.svg" alt="Checked Icon" />
									</span>
								</label>
							</div>

							<div>
								<input type="radio" id="color-4" name="color" value="color-4">
								<label for="color-4">
									<span>
										<img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/242518/check-icn.svg" alt="Checked Icon" />
									</span>
								</label>
							</div>
						</div>
						<div class="btn-container">
							<a href="" class="btn btn-cart">ADD TO CART</a>
						</div>
						<div class="sosial-icon">
							<a href=""><img src="<?=base_url()?>assets/front/img/fb.png"></a>
							<a href=""><img src="<?=base_url()?>assets/front/img/twitter.png"></a>
							<a href=""><img src="<?=base_url()?>assets/front/img/g-plus.png"></a>
						</div>
					</div>
				</div>
			</div>
		</div>


	</div>

	<section id="related-product">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<h2 class="title">RELATED PRODUCTS</h2>
					<p class="subtitle">YOU MAY ALSO LIKE</p>
				</div>
			</div>
		</div>
		<div class="box">
			<div class="product-container">
					<div class="col-lg-3 col-md-3 no-padding">
						<div class="flip-container" ontouchstart="this.classList.toggle('hover');">
							<div class="flipper">
								<div class="front">
									<img src="<?=base_url()?>assets/front/img/product.png">
									<a href="" class="icon-fav"><img src="<?=base_url()?>assets/front/img/fav.png"></a>
								</div>
								<div class="back">
									<div class="back-product"><img src="<?=base_url()?>assets/front/img/sapto.png"></div>
									<h4>Stretch Jersey Dress with Crystal Trim</h4>
									<p>$30</p>
									<span><a href="<?=site_url('store/detail')?>" class="btn btn-shop">SHOP</a></span>
									<a href="" class="icon-fav"><img src="<?=base_url()?>assets/front/img/fav.png"></a>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-3 col-md-3 no-padding">
						<div class="flip-container" ontouchstart="this.classList.toggle('hover');">
							<div class="flipper">
								<div class="front">
									<img src="<?=base_url()?>assets/front/img/product.png">
									<a href="" class="icon-fav"><img src="<?=base_url()?>assets/front/img/fav.png"></a>
								</div>
								<div class="back">
									<div class="back-product"><img src="<?=base_url()?>assets/front/img/sapto.png"></div>
									<h4>Stretch Jersey Dress with Crystal Trim</h4>
									<p>$30</p>
									<span><a href="<?=site_url('store/detail')?>" class="btn btn-shop">SHOP</a></span>
									<a href="" class="icon-fav"><img src="<?=base_url()?>assets/front/img/fav.png"></a>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-3 col-md-3 no-padding">
						<div class="flip-container" ontouchstart="this.classList.toggle('hover');">
							<div class="flipper">
								<div class="front">
									<img src="<?=base_url()?>assets/front/img/product.png">
									<a href="" class="icon-fav"><img src="<?=base_url()?>assets/front/img/fav.png"></a>
								</div>
								<div class="back">
									<div class="back-product"><img src="<?=base_url()?>assets/front/img/sapto.png"></div>
									<h4>Stretch Jersey Dress with Crystal Trim</h4>
									<p>$30</p>
									<span><a href="<?=site_url('store/detail')?>" class="btn btn-shop">SHOP</a></span>
									<a href="" class="icon-fav"><img src="<?=base_url()?>assets/front/img/fav.png"></a>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-3 col-md-3 no-padding">
						<div class="flip-container" ontouchstart="this.classList.toggle('hover');">
							<div class="flipper">
								<div class="front">
									<img src="<?=base_url()?>assets/front/img/product.png">
									<a href="" class="icon-fav"><img src="<?=base_url()?>assets/front/img/fav.png"></a>
								</div>
								<div class="back">
									<div class="back-product"><img src="<?=base_url()?>assets/front/img/sapto.png"></div>
									<h4>Stretch Jersey Dress with Crystal Trim</h4>
									<p>$30</p>
									<span><a href="<?=site_url('store/detail')?>" class="btn btn-shop">SHOP</a></span>
									<a href="" class="icon-fav"><img src="<?=base_url()?>assets/front/img/fav.png"></a>
								</div>
							</div>
						</div>
					</div>
				</div>
		</div>
	</section>


	<footer>
		<?php $this->load->view($this->theme . 'front/include/footer'); ?>
	</footer>

	<div id="search"> 
		<span class="close">X</span>
		<form role="search" id="searchform" action="/search" method="get">
			<input value="" name="q" type="search" placeholder="type to search"/>
		</form>
	</div>

	<!-- Modal -->
	<div id="register" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<form>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<div class="register-form">
					<div>
						<h1 class="text-center">Create an Account</h1>
						<div class="row">
							<div class="form-group col-lg-12">
								<label>EMAIL ADDRESS</label>
								<input type="email" name="" class="form-control" />
							</div>
							<div class="form-group col-lg-6">
								<label>PASSWORD</label>
								<input type="email" name="" class="form-control" />
							</div>
							<div class="form-group col-lg-6">
								<label>CONFIRM PASSWORD</label>
								<input type="email" name="" class="form-control" />
							</div>
							<div class="col-lg-12">
								<div class="form-group">
									<div class="flex">
										<div class="checkbox">
											<input type="checkbox" id="subscribe" name="" />
											<label for="subscribe"><span></span></label>
										</div>
										<div class="w-90 remember">
											<p class="black"></p>
											<p class="black">Subscribe to Saptodjojokartiko newsletter.</p>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-12">
								<button class="btn btn-sign-in">CREATE ACCOUNT</button>
							</div>
							<div class="col-lg-12">
								<p class="grey">By creating an account, you accept the terms of Saptodjojokartiko’s <a href="" class="black">Privacy Policy</a></p>
							</div>
						</div>
					</div>
				</div>
			</form>
			
		</div>
	</div>



	<script src="<?=base_url()?>assets/front/js/jquery-1.10.2.js"></script>
	<script src="<?=base_url()?>assets/front/js/bootstrap.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/front/js/template.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('a[href="#search"]').on('click', function(event) {                    
				$('#search').addClass('open');
				$('#search > form > input[type="search"]').focus();
			});            
			$('#search, #search button.close').on('click keyup', function(event) {
				if (event.target == this || event.target.className == 'close' || event.keyCode == 27) {
					$(this).removeClass('open');
				}
			});            
		});
	</script>

</body>
</html>