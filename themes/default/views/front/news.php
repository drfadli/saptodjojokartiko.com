<!DOCTYPE html>
<html>
<head>
	<?php $this->load->view($this->theme . 'front/include/head'); ?>


</head>
<body>
	<header>
		<?php $this->load->view($this->theme . 'front/include/header'); ?>
	</header>

    <section id="stori">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<p class="subtitle">NEW IN STORIES</p>
					<h2 class="title"><a href="<?=site_url('news')?>">NEWS & STORIES</a></h2>
					<p class="subtitle">NEW IN STORIES</p>
				</div>
				<div class="img-container">
					<div class="col-lg-4 col-md-6">
						<div class="img-stori">
							<img src="<?=base_url()?>assets/front/img/stori-1.jpg">
						</div>
						<div class="desc">
							<a href="<?=site_url('news/detail')?>"><p>TREASURE HUNTS : PHOTOS</p></a>
							<p>BY PETRA CHOLLINS</p>
						</div>
						<div class="line"></div>
						<div class="detail">
							<p>CATEGORY : EVENTS</p>
							<p>DATE : 11/12/18</p>
						</div>
					</div>
					<div class="col-lg-4 col-md-6">
						<div class="img-stori">
							<img src="<?=base_url()?>assets/front/img/stori-1.jpg">
						</div>
						<div class="desc">
							<a href="<?=site_url('news/detail')?>"><p>TREASURE HUNTS : PHOTOS</p></a>
							<p>BY PETRA CHOLLINS</p>
						</div>
						<div class="line"></div>
						<div class="detail">
							<p>CATEGORY : EVENTS</p>
							<p>DATE : 11/12/18</p>
						</div>
					</div>
					<div class="col-lg-4 col-md-6">
						<div class="img-stori">
							<img src="<?=base_url()?>assets/front/img/stori-1.jpg">
						</div>
						<div class="desc">
							<a href="<?=site_url('news/detail')?>"><p>TREASURE HUNTS : PHOTOS</p></a>
							<p>BY PETRA CHOLLINS</p>
						</div>
						<div class="line"></div>
						<div class="detail">
							<p>CATEGORY : EVENTS</p>
							<p>DATE : 11/12/18</p>
						</div>
					</div>
					<div class="col-lg-4 col-md-6">
						<div class="img-stori">
							<img src="<?=base_url()?>assets/front/img/stori-1.jpg">
						</div>
						<div class="desc">
							<a href="<?=site_url('news/detail')?>"><p>TREASURE HUNTS : PHOTOS</p></a>
							<p>BY PETRA CHOLLINS</p>
						</div>
						<div class="line"></div>
						<div class="detail">
							<p>CATEGORY : EVENTS</p>
							<p>DATE : 11/12/18</p>
						</div>
					</div>
					<div class="col-lg-4 col-md-6">
						<div class="img-stori">
							<img src="<?=base_url()?>assets/front/img/stori-1.jpg">
						</div>
						<div class="desc">
							<a href="<?=site_url('news/detail')?>"><p>TREASURE HUNTS : PHOTOS</p></a>
							<p>BY PETRA CHOLLINS</p>
						</div>
						<div class="line"></div>
						<div class="detail">
							<p>CATEGORY : EVENTS</p>
							<p>DATE : 11/12/18</p>
						</div>
					</div>
					<div class="col-lg-4 col-md-6">
						<div class="img-stori">
							<img src="<?=base_url()?>assets/front/img/stori-1.jpg">
						</div>
						<div class="desc">
							<a href="<?=site_url('news/detail')?>"><p>TREASURE HUNTS : PHOTOS</p></a>
							<p>BY PETRA CHOLLINS</p>
						</div>
						<div class="line"></div>
						<div class="detail">
							<p>CATEGORY : EVENTS</p>
							<p>DATE : 11/12/18</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<!-- Modal -->
	<div id="register" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<form>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<div class="register-form">
					<div>
						<h1 class="text-center">Create an Account</h1>
						<div class="row">
							<div class="form-group col-lg-12">
								<label>EMAIL ADDRESS</label>
								<input type="email" name="" class="form-control" />
							</div>
							<div class="form-group col-lg-6">
								<label>PASSWORD</label>
								<input type="email" name="" class="form-control" />
							</div>
							<div class="form-group col-lg-6">
								<label>CONFIRM PASSWORD</label>
								<input type="email" name="" class="form-control" />
							</div>
							<div class="col-lg-12">
								<div class="form-group">
									<div class="flex">
										<div class="checkbox">
											<input type="checkbox" id="subscribe" name="" />
											<label for="subscribe"><span></span></label>
										</div>
										<div class="w-90 remember">
											<p class="black"></p>
											<p class="black">Subscribe to Saptodjojokartiko newsletter.</p>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-12">
								<button class="btn btn-sign-in">CREATE ACCOUNT</button>
							</div>
							<div class="col-lg-12">
								<p class="grey">By creating an account, you accept the terms of Saptodjojokartiko’s <a href="" class="black">Privacy Policy</a></p>
							</div>
						</div>
					</div>
				</div>
			</form>
			
		</div>
	</div>
	
	<footer>
		<?php $this->load->view($this->theme . 'front/include/footer'); ?>
	</footer>

	<div id="search"> 
		<span class="close">X</span>
		<form role="search" id="searchform" action="/search" method="get">
			<input value="" name="q" type="search" placeholder="type to search"/>
		</form>
	</div>

	<script src="<?=base_url()?>assets/front/js/jquery-1.10.2.js"></script>
	<script src="<?=base_url()?>assets/front/js/bootstrap.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/front/js/template.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('a[href="#search"]').on('click', function(event) {                    
				$('#search').addClass('open');
				$('#search > form > input[type="search"]').focus();
			});            
			$('#search, #search button.close').on('click keyup', function(event) {
				if (event.target == this || event.target.className == 'close' || event.keyCode == 27) {
					$(this).removeClass('open');
				}
			});            
		});
	</script>

</body>
</html>