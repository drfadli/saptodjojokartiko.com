<!DOCTYPE html>
<html>
<head>
	<?php $this->load->view($this->theme . 'front/include/head'); ?>


	<style>
		ul {
				 list-style-type: square;
			 	margin-left: 15px; /* To remove default bottom margin */ 
    			
			}
	</style>

</head>
<body>
	<header>
		<?php $this->load->view($this->theme . 'front/include/header'); ?>
	</header>

<section id="detail">
		<div class="flex">
			<div class="w-50">
					  <div id="myCarousel" class="carousel slide" data-ride="carousel">
					    <!-- Indicators -->
					    <ol class="carousel-indicators">
					      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
					      <li data-target="#myCarousel" data-slide-to="1"></li>
					      <li data-target="#myCarousel" data-slide-to="2"></li>
					    </ol>

					    <!-- Wrapper for slides -->
					    <div class="carousel-inner">
						      <div class="item active">
						        <img src="<?=base_url()?>assets/front/img/blog/antinoelon.jpg">
						      </div>

						      <div class="item">
						        <img src="<?=base_url()?>assets/front/img/blog/antinoelon.jpg">
						      </div>
						    
						      <div class="item">
						        <img src="<?=base_url()?>assets/front/img/blog/antinoelon.jpg">
						      </div>
						    </div>

					  </div>

			</div>
			<div class="w-50" style="margin-top: 50px">
				<p class="title-blog" style="font-size:30px">Stretch Jersey Dress <br>with Crystal Trim</p>
						<br>
						<p style="width: 400px">
							A short sleeve dress with a round neck made in stretch viscose jersey. The neckline is trimmed with pressed fabric in a torchon shape outlined by crystals. The bow continues to be a defining feature of the House placed here on the waist. 
						</p>
							<ul>
								<li>Black compact stretch viscose jersey</li>
								<li>Pressed stretch viscose jersey torchon with crystal trim</li>
								<li>Back zip closure</li>
								<li>75% viscose, 17% nylon and 8% elastane</li>
								<li>Made in Italy</li>
								<li>the model is 5’9" and is wearing a size small</li>
							</ul>
			</div>
		</div>
	</section>
	<section id="stori">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<h2 class="title">RELATED ARTICLES</h2>
				</div>
				<div class="img-container">
					<div class="col-lg-3 col-md-6">
						<div class="img-stori">
							<img src="<?=base_url()?>assets/front/img/stori-1.jpg">
						</div>
						<div class="desc">
							<a href=""><p>TREASURE HUNTS : PHOTOS</p></a>
							<p>BY PETRA CHOLLINS</p>
						</div>
						<div class="line"></div>
						<div class="detail">
							<p>CATEGORY : EVENTS</p>
							<p>DATE : 11/12/18</p>
						</div>
					</div>
					<div class="col-lg-3 col-md-6">
						<div class="img-stori">
							<img src="<?=base_url()?>assets/front/img/stori-1.jpg">
						</div>
						<div class="desc">
							<a href=""><p>TREASURE HUNTS : PHOTOS</p></a>
							<p>BY PETRA CHOLLINS</p>
						</div>
						<div class="line"></div>
						<div class="detail">
							<p>CATEGORY : EVENTS</p>
							<p>DATE : 11/12/18</p>
						</div>
					</div>
					<div class="col-lg-3 col-md-6">
						<div class="img-stori">
							<img src="<?=base_url()?>assets/front/img/stori-1.jpg">
						</div>
						<div class="desc">
							<a href=""><p>TREASURE HUNTS : PHOTOS</p></a>
							<p>BY PETRA CHOLLINS</p>
						</div>
						<div class="line"></div>
						<div class="detail">
							<p>CATEGORY : EVENTS</p>
							<p>DATE : 11/12/18</p>
						</div>
					</div>
					<div class="col-lg-3 col-md-6">
						<div class="img-stori">
							<img src="<?=base_url()?>assets/front/img/stori-1.jpg">
						</div>
						<div class="desc">
							<a href=""><p>TREASURE HUNTS : PHOTOS</p></a>
							<p>BY PETRA CHOLLINS</p>
						</div>
						<div class="line"></div>
						<div class="detail">
							<p>CATEGORY : EVENTS</p>
							<p>DATE : 11/12/18</p>
						</div>
					</div>
				</div>
			</div>
		</div>

	</section>

	<footer>
		<?php $this->load->view($this->theme . 'front/include/footer'); ?>
	</footer>

	<div id="search"> 
		<span class="close">X</span>
		<form role="search" id="searchform" action="/search" method="get">
			<input value="" name="q" type="search" placeholder="type to search"/>
		</form>
	</div>

	<script src="<?=base_url()?>assets/front/js/jquery-1.10.2.js"></script>
	<script src="<?=base_url()?>assets/front/js/bootstrap.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/front/js/template.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('a[href="#search"]').on('click', function(event) {                    
				$('#search').addClass('open');
				$('#search > form > input[type="search"]').focus();
			});            
			$('#search, #search button.close').on('click keyup', function(event) {
				if (event.target == this || event.target.className == 'close' || event.keyCode == 27) {
					$(this).removeClass('open');
				}
			});            
		});
	</script>

</body>
</html>