<!DOCTYPE html>
<html>
<head>
	<?php $this->load->view($this->theme . 'front/include/head'); ?>


</head>
<body>
	<header>
		<?php $this->load->view($this->theme . 'front/include/header'); ?>
	</header>


	<section id="sign-in-section">
		<div class="container">
			<div class="row">

				<div class="col-6 col-lg-6 col-md-6">
					<form method="" action="welcome.html">
						<div class="left-section">
							<h2>Sign in</h2>
							<div class="form-group">
								<label>EMAIL ADDRESS</label>
								<input type="email" name="" class="form-control" required />
							</div>
							<div class="form-group">
								<label>PASSWORD</label>
								<input type="password" name="" class="form-control" />
							</div>
							<div class="form-group">
								<div class="flex">
									<div class="checkbox">
										<input type="checkbox" id="c1" name="" />
										<label for="c1"><span></span></label>
									</div>
									<div class="w-90 remember">
										<p class="black"><strong>Subscribe to Saptodjojokartiko newsletter.</strong></p>
										<p class="grey">By choosing continue below, you accept the terms of our <a href="" class="black"><strong>privacy policy.</strong></a></p>
									</div>
								</div>
							</div>
							<div class="form-group">
								<button class="btn btn-signin">SIGN IN</button>
							</div>
							<p><a href="" type="button" data-toggle="modal" data-target="#forgot-password" class="black">Forgot Password?</a></p>
						</div>
					</form>
				</div>
				<div class="col-6 col-lg-6 col-md-6">
					<div class="right-section">
						<h2>Don’t have an account</h2>
						<p>Having a Saptodjojokartiko account will give you access to:</p>
						<ul>
							<li>• Saved items in your wishlist</li>
							<li>• Personalized recommendations.</li>
							<li>• Order delivery updates and return management.</li>
						</ul>
						<div class="form-group">
							<button class="btn btn-signup" data-toggle="modal" data-target="#register">CREATE ACCOUNT</button>
						</div>
					</div>
				</div>

			</div>
		</div>
	</section>

	<footer>
		<?php $this->load->view($this->theme . 'front/include/footer'); ?>
	</footer>

	<div id="search"> 
		<span class="close">X</span>
		<form role="search" id="searchform" action="/search" method="get">
			<input value="" name="q" type="search" placeholder="type to search"/>
		</form>
	</div>

	<!-- Modal -->
	<div id="register" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<form action="<?=site_url('account/welcome')?>">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<div class="register-form">
					<div>
						<h1 class="text-center">Create an Account</h1>
						<div class="row">
							<div class="form-group col-lg-12">
								<label>EMAIL ADDRESS</label>
								<input type="email" name="" class="form-control" />
							</div>
							<div class="form-group col-lg-6">
								<label>PASSWORD</label>
								<input type="email" name="" class="form-control" />
							</div>
							<div class="form-group col-lg-6">
								<label>CONFIRM PASSWORD</label>
								<input type="email" name="" class="form-control" />
							</div>
							<div class="col-lg-12">
								<div class="form-group">
									<div class="flex">
										<div class="checkbox">
											<input type="checkbox" id="subscribe" name="" />
											<label for="subscribe"><span></span></label>
										</div>
										<div class="w-90 remember">
											<p class="black"></p>
											<p class="black">Subscribe to Saptodjojokartiko newsletter.</p>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-12">
								<button class="btn btn-sign-in">CREATE ACCOUNT</button>
							</div>
							<div class="col-lg-12">
								<p class="grey">By creating an account, you accept the terms of Saptodjojokartiko’s <a href="" class="black">Privacy Policy</a></p>
							</div>
						</div>
					</div>
				</div>
			</form>
			
		</div>
	</div>

	<!-- Modal -->
	<div id="forgot-password" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<form>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<div class="register-form">
					<div>
						<h1 class="text-center">Forgotten Password</h1>
						<p class="subtitle text-center">Please enter the email address you registered with, and we will send
						you a link to reset your password.</p>
						<div class="row">
							<div class="form-group col-lg-12">
								<label>EMAIL ADDRESS</label>
								<input type="email" name="" class="form-control" />
							</div>
							<div class="col-lg-12">
								<button class="btn btn-sign-in">SUBMIT</button>
							</div>
						</div>
					</div>
				</div>
			</form>
			
		</div>
	</div>


	<script src="<?=base_url()?>assets/front/js/jquery-1.10.2.js"></script>
	<script src="<?=base_url()?>assets/front/js/bootstrap.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/front/js/template.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('a[href="#search"]').on('click', function(event) {                    
				$('#search').addClass('open');
				$('#search > form > input[type="search"]').focus();
			});            
			$('#search, #search button.close').on('click keyup', function(event) {
				if (event.target == this || event.target.className == 'close' || event.keyCode == 27) {
					$(this).removeClass('open');
				}
			});            
		});
	</script>

</body>
</html>