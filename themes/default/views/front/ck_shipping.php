<!DOCTYPE html>
<html>
<head>
	<?php $this->load->view($this->theme . 'front/include/head'); ?>

</head>
<body>
	<header>
		<?php $this->load->view($this->theme . 'front/include/header'); ?>
	</header>


	<section id="shipping">
		<div class="container">
			<div class="row">

				<div class="col-lg-8">
					<div class="content">
						<a href="<?=site_url('checkout/shipping?step=shipping')?>"><h1  class="<?php if($this->input->get('step')=="shipping") { echo "black"; }?>">1. Shipping Address</h1></a>

						<?php if($this->input->get('step')=="shipping") { ?>
						<p class="subtitle grey text-left">All fields are required excep if mentioned optional</p>

						<h5 class="black">SHIP TO</h5>
						<input type="radio" id="Save-shipping-address-information-to-address-book" name="" />
						<label for="Save-shipping-address-information-to-address-book"><span></span> New Address</label>

						<div class="shipping-address">
							<!-- FORM-->
							<div class="row">
								<form method="" action="shipping-2.html">
									<div class="form-group col-lg-2">
										<label>TITLE</label>
										<select class="form-control">
											<option>MR</option>
										</select>
									</div>
									<div class="form-group col-lg-5">
										<label>FIRST NAME</label>
										<input type="text" name="" class="form-control" />
									</div>
									<div class="form-group col-lg-5">
										<label>LAST NAME</label>
										<input type="text" name="" class="form-control" />
									</div>

									<div class="form-group col-lg-12">
										<label>ADDRESS LINE 1</label>
										<input type="text" name="" class="form-control" />
									</div>
									<div class="form-group col-lg-12">
										<label>ADDRESS LINE 2</label>
										<input type="text" name="" class="form-control" />
									</div>

									<div class="form-group col-lg-6">
										<label>CITY</label>
										<input type="text" name="" class="form-control" />
									</div>
									<div class="form-group col-lg-3">
										<label>STATE</label>
										<input type="text" name="" class="form-control" />
									</div>
									<div class="form-group col-lg-3">
										<label>ZIP CODE</label>
										<input type="text" name="" class="form-control" />
									</div>

									<div class="col-lg-12">
										<div class="form-group" style="margin-bottom: 40px;">
											<div class="flex">
												<div class="checkbox">
													<input type="checkbox" id="This-is-a-business-address" name="" />
													<label for="This-is-a-business-address"><span></span></label>
												</div>
												<div class="w-90 remember">
													<p class="black"></p>
													<p class="black">This is a business address</p>
												</div>
											</div>
										</div>
									</div>

									<div class="form-group col-lg-6">
										<label>CONTACT PHONE NUMBER</label>
										<input type="text" name="" class="form-control" />
									</div>
									<div class="form-group col-lg-6">
										<label>.</label>
										<input type="text" name="" class="form-control" />
									</div>

									<div class="col-lg-12">
										<div class="form-group">
											<div class="flex">
												<div class="checkbox">
													<input type="checkbox" id="Add-addtional-contact-number" name="" />
													<label for="Add-addtional-contact-number"><span></span></label>
												</div>
												<div class="w-90 remember">
													<p class="black"></p>
													<p class="black">Add addtional contact number</p>
												</div>
											</div>
											<div class="flex">
												<div class="checkbox">
													<input type="checkbox" id="Save-shipping-address-information-to-address-book" name="" />
													<label for="Save-shipping-address-information-to-address-book"><span></span></label>
												</div>
												<div class="w-90 remember">
													<p class="black"></p>
													<p class="black">Save shipping address information to address book</p>
												</div>
											</div>
										</div>
									</div>

									<div class="col-lg-12">
										<button class="btn btn-checkout">CONTINUE PAYMENT</button>
										<p class="grey">All items ship in saptodjojokartiko signature packaging</p>
									</div>
								</form>
							</div>
							<!--/FORM-->
						</div>
						<?php } else { ?>

							<div class="shipping-address">
							<div class="row">
								<div class="col-xs-6">
									<p class="black">Mr. Riki</p>
									<p>Puri Bintaro PB 14 No 30</p>
									<p>tangerang 15229</p>
									<p>+ xxx xxx xxx</p>
								</div>
								<div class="col-xs-6">
									<p class="black">Two Day</p>
									<p>$ 0</p>
									<p>you will be notified when your item is shipped</p>
								</div>
							</div>
						</div>

						<?php } ?>


						<div class="footer-shipping">
							<div class="row">
								<div class="col-lg-12">
									<div class="gifting">
										<h1>2. Gifting</h1>
									</div>
								</div>
								<div class="col-lg-12">

									<div class="payment">
										<a href="<?=site_url('checkout/shipping?step=payment')?>"><h1  class="<?php if($this->input->get('step')=="payment") { echo "black"; }?>">3. Payment</h1></a>
									</div>

									<?php if($this->input->get('step')=="payment") { ?>
									<div class="desc-payment">
										<div class="row">

											<div class="form-group col-lg-3">
												<label>CREDIT CARD TYPE</label>
												<input type="text" name="" placeholder="" class="form-control" />
											</div>
											<div class="form-group col-lg-6">
												<label>CREDIT CARD NUMBER</label>
												<input type="text" name="" placeholder="" class="form-control" />
											</div>
											<div class="form-group col-lg-3">
												<label>SECURITY CODE</label>
												<input type="text" name="" placeholder="" class="form-control" />
											</div>


											<div class="form-group col-lg-6">
												<label>NAME ON CARD</label>
												<input type="text" name="" placeholder="" class="form-control" />
											</div>
											<div class="form-group col-lg-3">
												<label>EXP DATE</label>
												<input type="text" name="" placeholder="MM" class="form-control" />
											</div>
											<div class="form-group col-lg-3">
												<label>.</label>
												<input type="text" name="" placeholder="YYYY" class="form-control" />
											</div>


											<div class="col-lg-12">
												<div class="form-group">
													<div class="flex">
														<div class="checkbox">
															<input type="checkbox" id="Save-credit-card-information-to-wallet" name="" />
															<label for="Save-credit-card-information-to-wallet"><span></span></label>
														</div>
														<div class="w-90 remember">
															<p class="black"></p>
															<p class="grey">Save credit card information to wallet</p>
														</div>
													</div>
													<div class="flex">
														<div class="checkbox">
															<input type="checkbox" id="Save-shipping-address-information-to-address-book" name="" />
															<label for="Save-shipping-address-information-to-address-book"><span></span></label>
														</div>
														<div class="w-90 remember">
															<p class="grey">Billing address and phone are tha same as shipping information</p><br/>
															<p class="black">If the billing address and / or billing phone associated with your <br />
															payment method are the same, please check this box.</p>
														</div>
													</div>
												</div>
											</div>

											<div class="col-lg-12">
												<p style="margin: 20px 0px 40px 0px;">Please enter the corresponding billing information for the payment method you intend to use.</p>
											</div>


											<div class="form-group col-lg-3">
												<label>COUNTRY</label>
												<input type="text" name="" placeholder="" class="form-control" />
											</div>
											<div class="clearfix"></div>

											<div class="form-group col-lg-2">
												<label>TITLE</label>
												<select class="form-control">
													<option>MR</option>
												</select>
											</div>
											<div class="form-group col-lg-5">
												<label>FIRST NAME</label>
												<input type="text" name="" class="form-control" />
											</div>
											<div class="form-group col-lg-5">
												<label>LAST NAME</label>
												<input type="text" name="" class="form-control" />
											</div>

											<div class="form-group col-lg-12">
												<label>ADDRESS LINE 1</label>
												<input type="text" name="" class="form-control" />
											</div>
											<div class="form-group col-lg-12">
												<label>ADDRESS LINE 2</label>
												<input type="text" name="" class="form-control" />
											</div>

											<div class="form-group col-lg-6">
												<label>CITY</label>
												<input type="text" name="" class="form-control" />
											</div>
											<div class="form-group col-lg-3">
												<label>STATE</label>
												<input type="text" name="" class="form-control" />
											</div>
											<div class="form-group col-lg-3">
												<label>ZIP CODE</label>
												<input type="text" name="" class="form-control" />
											</div>

											<div class="col-lg-12">
												<div class="form-group" style="margin-bottom: 40px;">
													<div class="flex">
														<div class="checkbox">
															<input type="checkbox" id="This-is-a-business-address" name="" />
															<label for="This-is-a-business-address"><span></span></label>
														</div>
														<div class="w-90 remember">
															<p class="black"></p>
															<p class="black">This is a business address</p>
														</div>
													</div>
												</div>
											</div>

											<div class="form-group col-lg-6">
												<label>CONTACT PHONE NUMBER</label>
												<input type="text" name="" class="form-control" />
											</div>
											<div class="form-group col-lg-6">
												<label>.</label>
												<input type="text" name="" class="form-control" />
											</div>

											<div class="col-lg-12">
												<div class="form-group">
													<div class="flex">
														<div class="checkbox">
															<input type="checkbox" id="Add-addtional-contact-number" name="" />
															<label for="Add-addtional-contact-number"><span></span></label>
														</div>
														<div class="w-90 remember">
															<p class="black"></p>
															<p class="black">Add addtional contact number</p>
														</div>
													</div>
												</div>
											</div>

											<div class="col-lg-12">
												<a href="" class="btn btn-checkout">CONTINUE PAYMENT</a>
											</div>

										</div>
									</div>
									<?php } ?>
								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- col-lg-4 -->
				<div class="col-lg-4">
					<!-- sidebar -->
					<div class="side-bar">

						<div class="troli text-center">
							<img src="<?=base_url()?>assets/front/img/troli.png"> <span class="grey">1 Item</span>
						</div>
						<div class="flex">
							<div class="img-checkout">
								<img src="<?=base_url()?>assets/front/img/baju.png">
							</div>
							<div class="desc-checkout">
								<p class="black">Stretch Jersey Dress</p>
								<p>with Crystal Trim</p>
								<p>Style xxx xxxxx xxx</p>
								<p>Qty : 1</p>
								<p>Size: Small</p>

								<br />

								<p>AVAILABLE</p>
								<p>Your selection is available</p>
								<p>for immediate purchase</p>
								<p>online.</p>

								<br />

								<p>Price : $ 3,400</p>
							</div>
						</div>
						<div class="subtotal flex">
							<div class="w-80">
								<p>Subtotal</p>
								<p>Shipping</p>
								<p>Estimated Tax</p>
								<p>Estimated Total</p>
							</div>
							<div class="w-20">
								<p class="text-left">$ 3,400</p>
								<p class="text-left">Free</p>
								<p class="text-left">$ 0.00</p>
								<p class="text-left">$ 3,400</p>
							</div>
						</div>
						<div class="flex">
							<div class="checkbox">
								<input type="checkbox" id="Save-shipping-address-information-to-address-book" name="" />
								<label for="Save-shipping-address-information-to-address-book"><span></span></label>
							</div>
							<div class="w-90 remember">
								<p class="black"></p>
								<p class="grey">I accept Saptodjojokartiko’s terms & Cond</p>
							</div>
						</div>

						<button class="btn btn-order col-lg-12">PLACE ORDER</button>
						<div class="clearfix"></div>

						<!-- panell -->
						<div class="panel-desc">
							<!-- panell 1 -->
							<div class="panel-group" id="accordion">
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#VIEW-DETAILS">
												VIEW DETAILS
											</a>
										</h4>
									</div>
									<div id="VIEW-DETAILS" class="panel-collapse collapse in">
										<div class="panel-body">
											<p class="grey">Please note that you will not be charged for item in your
											order until they have been shipped to you.</p>
										</div>
									</div>
								</div>
								<!-- /panell 1 -->
								<!-- panell 2 -->
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#MAY-WE-HELP">
												MAY WE HELP
											</a>
										</h4>
									</div>
									<div id="MAY-WE-HELP" class="panel-collapse collapse">
										<div class="panel-body">
											<p class="grey"><img src="<?=base_url()?>assets/front/img/phone.png"> <span>+ XXX XXX XXX</span></p>
											<p class="grey"><img src="<?=base_url()?>assets/front/img/email.png"> <span>info@gmail.com</span></p>
										</div>
									</div>
								</div>
								<!-- /panell 2-->
								<!-- panell 3-->
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#PAYMENT-OPTIONS">
												PAYMENT OPTIONS
											</a>
										</h4>
									</div>
									<div id="PAYMENT-OPTIONS" class="panel-collapse collapse">
										<div class="panel-body">
											<p class="grey">Please note that you will not be charged for item in your
											order until they have been shipped to you.</p>
										</div>
									</div>
								</div>
								<!-- /panell 3-->
								<!-- panell 4-->
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#SHIPPING-OPTIONS">
												SHIPPING OPTIONS
											</a>
										</h4>
									</div>
									<div id="SHIPPING-OPTIONS" class="panel-collapse collapse">
										<div class="panel-body">
											<p class="grey">Please note that you will not be charged for item in your
											order until they have been shipped to you.</p>
										</div>
									</div>
								</div>
								<!-- /panell 4-->
							</div>
							<!--/#panell -->
						</div>

					</div>
					<!-- sidebar -->
				</div>
				<!-- /col-lg-4 -->

			</div>
		</div>
	</section>

	<footer>
		<?php $this->load->view($this->theme . 'front/include/footer'); ?>
	</footer>


	<div id="search"> 
		<span class="close">X</span>
		<form role="search" id="searchform" action="/search" method="get">
			<input value="" name="q" type="search" placeholder="type to search"/>
		</form>
	</div>


	<!-- Modal -->
	<div id="register" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<form>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<div class="register-form">
					<div>
						<h1 class="text-center">Create an Account</h1>
						<div class="row">
							<div class="form-group col-lg-12">
								<label>EMAIL ADDRESS</label>
								<input type="email" name="" class="form-control" />
							</div>
							<div class="form-group col-lg-6">
								<label>PASSWORD</label>
								<input type="email" name="" class="form-control" />
							</div>
							<div class="form-group col-lg-6">
								<label>CONFIRM PASSWORD</label>
								<input type="email" name="" class="form-control" />
							</div>
							<div class="col-lg-12">
								<div class="form-group">
									<div class="flex">
										<div class="checkbox">
											<input type="checkbox" id="subscribe" name="" />
											<label for="subscribe"><span></span></label>
										</div>
										<div class="w-90 remember">
											<p class="black"></p>
											<p class="black">Subscribe to Saptodjojokartiko newsletter.</p>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-12">
								<button class="btn btn-sign-in">CREATE ACCOUNT</button>
							</div>
							<div class="col-lg-12">
								<p class="grey">By creating an account, you accept the terms of Saptodjojokartiko’s <a href="" class="black">Privacy Policy</a></p>
							</div>
						</div>
					</div>
				</div>
			</form>
			
		</div>
	</div>


	<script src="<?=base_url()?>assets/front/js/jquery-1.10.2.js"></script>
	<script src="<?=base_url()?>assets/front/js/bootstrap.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/front/js/template.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('a[href="#search"]').on('click', function(event) {                    
				$('#search').addClass('open');
				$('#search > form > input[type="search"]').focus();
			});            
			$('#search, #search button.close').on('click keyup', function(event) {
				if (event.target == this || event.target.className == 'close' || event.keyCode == 27) {
					$(this).removeClass('open');
				}
			});            
		});
	</script>

</body>
</html>