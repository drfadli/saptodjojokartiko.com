<!DOCTYPE html>
<html>
<head>
	<?php $this->load->view($this->theme . 'front/include/head'); ?>

</head>
<body>
	<header>
		<?php $this->load->view($this->theme . 'front/include/header'); ?>

	<section id="blog">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<h2 class="title">ABOUT US</h2>
				</div>
				<div class="page" style="width:90%; padding:10px; margin: 0 auto; margin-top:50px; margin-bottom:100px">
                    <p>Influential, innovative and progressive, Gucci is reinventing a wholly modern approach to fashion. Under the new vision of creative director Alessandro Michele, the House has redefined luxury for the 21st century, further reinforcing its position as one of the world’s most desirable fashion houses. Eclectic, contemporary, romantic—Gucci products represent the pinnacle of Italian craftsmanship and are unsurpassed for their quality and attention to detail. </p>
                    <p></p>Gucci is part of the Kering Group, a world leader in apparel and accessories that owns a portfolio of powerful luxury and sport and lifestyle brands. </p>
                    <p></p>Discover the stories behind Alessandro Michele's collections, exclusively on Stories.</p>
				</div>
			</div>
			
		</div>
	</div>
	</section>
	
	<footer>
		<?php $this->load->view($this->theme . 'front/include/footer'); ?>
	</footer>

	<div id="search"> 
		<span class="close">X</span>
		<form role="search" id="searchform" action="/search" method="get">
			<input value="" name="q" type="search" placeholder="type to search"/>
		</form>
	</div>

	<script src="<?=base_url()?>assets/front/js/jquery-1.10.2.js"></script>
	<script src="<?=base_url()?>assets/front/js/bootstrap.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/front/js/template.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('a[href="#search"]').on('click', function(event) {                    
				$('#search').addClass('open');
				$('#search > form > input[type="search"]').focus();
			});            
			$('#search, #search button.close').on('click keyup', function(event) {
				if (event.target == this || event.target.className == 'close' || event.keyCode == 27) {
					$(this).removeClass('open');
				}
			});            
		});
	</script>

</body>
</html>