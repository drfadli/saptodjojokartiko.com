<!DOCTYPE html>
<html>
<head>
	<?php $this->load->view($this->theme . 'front/include/head'); ?>

</head>
<body>


	<section id="header">
		<nav class="navbar navbar-expand-lg">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed toggle-left" data-toggle="collapse" data-target="#navbar7">
						<input type="checkbox" id="h-icon" class="toggle"/>
						<label for="h-icon" class="hamburger">
							<span class="hamburger-line hamburger-line--top"></span>
							<span class="hamburger-line hamburger-line--middle"></span>
							<span class="hamburger-line hamburger-line--bottom"></span>
						</label>
					</button>
					<a class="navbar-brand" href="<?=site_url()?>"><img src="<?=base_url()?>assets/front/img/logo.png" alt="Dispute Bills">
					</a>
				</div>

				<div id="navbar7" class="navbar-collapse collapse">
					<ul class="nav navbar-nav navbar-left">
						<li class="nav-item active">
							<a class="nav-link" href="#">SHIPPING TO : INDONESIA <img src="<?=base_url()?>assets/front/img/web.png"></a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#">CUSTOMER CARE <img src="<?=base_url()?>assets/front/img/mail.png"></a>
						</li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<li class="nav-item active">
							<a class="nav-link" href="#search">Search<img src="<?=base_url()?>assets/front/img/search.png"></a>
						</li>
						<li class="nav-item">
							<a class="nav-link dropdown-toggle" type="button" data-toggle="dropdown" data-target="#sign-in" href="#">Login / register <img src="<?=base_url()?>assets/front/img/person.png"></a>
							<div id="sign-in">
								<ul class="dropdown-menu sign-in" style="position: absolute;right: -10px;">
									<li class="detail-checkout">
										<div class="desc-sign">
											Create an account for access to
											<ul>
												<li>• Saved items in your wishlist</li>
												<li>• Personalized recommendations.</li>
												<li>• Order delivery updates and return management.</li>
											</ul>
										</div>
									</li>
									<li class="footer-checkout"><a href="<?=site_url('account/signin')?>" class="btn btn-sign-in">SIGN <INS></INS></a></li>
									<li class="footer-checkout"><a href="#" type="button" data-toggle="modal" data-target="#register" class="btn btn-sign-up">CREATE ACCOUNT</a></li>
								</ul>
							</div>
							<div id="sign-in-menu" class="">
								<ul class="dropdown-menu" style="position: absolute;right: -10px;">
									<li><a href="" class="header-checkout">My Account</a></li>
									<li class="divider"></li>
									<li><a href="">My Orders</a></li>
									<li class="divider"></li>
									<li><a href="">Saved Items <img src="assets/front/img/fav.png"></a></li>
									<li class="divider"></li>
									<li><a href="">Recommended</a></li>
									<li class="divider"></li>
									<li><a href="">Account Settings</a></li>
									<li class="divider"></li>
									<li><a href="">Address Book</a></li>
									<li class="divider"></li>
									<li><a href="">Wallet</a></li>
									<li class="divider"></li>
									<li><a href="">Sign out</a></li>
								</ul>
							</div>
						</li>
						<li class="nav-item">
							<a class="nav-link dropdown-toggle" type="button" data-toggle="dropdown" href="#">Shopping bag <img src="assets/front/img/cart.png"></a>
							<ul class="dropdown-menu" style="position: absolute;right: 15px;">
								<li class="header-checkout"><h5>SHOOPING BAG</h5></li>
								<li class="divider"></li>
								<li class="detail-checkout">
									<div class="img-checkout">
										<img src="assets/front/img/baju.png">
									</div>
									<div class="desc-checkout">
										<p class="title-desc-checkout">Stretch Jersey Dress with Crystal Trim</p>
										<p>Style xxx xxxxx xxx</p>
										<p>Qty : 1</p>
										<p>Price : $ 3,400</p>
										<p>Size: Small</p>
									</div>
								</li>
								<li class="divider"></li>
								<li class="price-checkout">
									<p class="text-left">Subtotal</p>
									<h4 class="text-right">$3,400</h4>
								</li>
								<li class="divider"></li>
								<li class="footer-checkout"><a href="<?=site_url('checkout')?>" class="btn btn-checkout">checkout</a></li>
							</ul>
						</li>
					</ul>
					<div id="navbar-primary">
						<ul class="nav navbar-nav">
							<li class="nav-item active">
								<a class="nav-link" href="<?=site_url('store')?>">STORE</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="<?=site_url('news')?>">NEWS & STORIES</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="<?=site_url('collection')?>">COLLECTION</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="<?=site_url('gift')?>">BRIDE</a>
							</li>
						</ul>
					</div>
				</div><!-- /.navbar-collapse -->
				<!--/.nav-collapse -->
			</div>
			<!--/.container-fluid -->
		</nav>
	</section>

	<div class="cart">

	</div>

	<section id="new-arrivals">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<p class="subtitle">NEW IN STORE</p>
					<h2 class="title"><a href="<?=site_url('store')?>">LATEST & NEW ARRIVALS</a></h2>
					<p class="subtitle">COLLECTIONS & BRIDALS</p>
				</div>
			</div>
		</div>
		<div class="img-container">
			<div class="img-arrivals">
				<a href="">
					<img src="<?=base_url()?>assets/front/img/new-arrivals1.jpg">
				</a>
			</div>
			<div class="img-arrivals">
				<a href="">
					<img src="<?=base_url()?>assets/front/img/new-arrivals2.jpg">
				</a>
			</div>
		</div>
	</section>

	<section id="stori">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<p class="subtitle">NEW IN STORIES</p>
					<h2 class="title"><a href="<?=site_url('news')?>">NEWS & STORIES</a></h2>
					<p class="subtitle">NEW IN STORIES</p>
				</div>
				<div class="img-container">
					<div class="col-lg-3 col-md-6">
						<div class="img-stori">
							<img src="<?=base_url()?>assets/front/img/stori-1.jpg">
						</div>
						<div class="desc">
							<a href="<?=site_url('news/detail')?>"><p>TREASURE HUNTS : PHOTOS</p></a>
							<p>BY PETRA CHOLLINS</p>
						</div>
						<div class="line"></div>
						<div class="detail">
							<p>CATEGORY : EVENTS</p>
							<p>DATE : 11/12/18</p>
						</div>
					</div>
					<div class="col-lg-3 col-md-6">
						<div class="img-stori">
							<img src="<?=base_url()?>assets/front/img/stori-1.jpg">
						</div>
						<div class="desc">
							<a href="<?=site_url('news/detail')?>"><p>TREASURE HUNTS : PHOTOS</p></a>
							<p>BY PETRA CHOLLINS</p>
						</div>
						<div class="line"></div>
						<div class="detail">
							<p>CATEGORY : EVENTS</p>
							<p>DATE : 11/12/18</p>
						</div>
					</div>
					<div class="col-lg-3 col-md-6">
						<div class="img-stori">
							<img src="<?=base_url()?>assets/front/img/stori-1.jpg">
						</div>
						<div class="desc">
							<a href="<?=site_url('news/detail')?>"><p>TREASURE HUNTS : PHOTOS</p></a>
							<p>BY PETRA CHOLLINS</p>
						</div>
						<div class="line"></div>
						<div class="detail">
							<p>CATEGORY : EVENTS</p>
							<p>DATE : 11/12/18</p>
						</div>
					</div>
					<div class="col-lg-3 col-md-6">
						<div class="img-stori">
							<img src="<?=base_url()?>assets/front/img/stori-1.jpg">
						</div>
						<div class="desc">
							<a href="<?=site_url('news/detail')?>"><p>TREASURE HUNTS : PHOTOS</p></a>
							<p>BY PETRA CHOLLINS</p>
						</div>
						<div class="line"></div>
						<div class="detail">
							<p>CATEGORY : EVENTS</p>
							<p>DATE : 11/12/18</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>


	<footer>
		<?php $this->load->view($this->theme . 'front/include/footer'); ?>
	</footer>

	<div id="search"> 
		<span class="close">X</span>
		<form role="search" id="searchform" action="/search" method="get">
			<input value="" name="q" type="search" placeholder="type to search"/>
		</form>
	</div>

	<!-- Modal -->
	<div id="register" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<form>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<div class="register-form">
					<div>
						<h1 class="text-center">Create an Account</h1>
						<div class="row">
							<div class="form-group col-lg-12">
								<label>EMAIL ADDRESS</label>
								<input type="email" name="" class="form-control" />
							</div>
							<div class="form-group col-lg-6">
								<label>PASSWORD</label>
								<input type="email" name="" class="form-control" />
							</div>
							<div class="form-group col-lg-6">
								<label>CONFIRM PASSWORD</label>
								<input type="email" name="" class="form-control" />
							</div>
							<div class="col-lg-12">
								<div class="form-group">
									<div class="flex">
										<div class="checkbox">
											<input type="checkbox" id="subscribe" name="" />
											<label for="subscribe"><span></span></label>
										</div>
										<div class="w-90 remember">
											<p class="black"></p>
											<p class="black">Subscribe to Saptodjojokartiko newsletter.</p>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-12">
								<button class="btn btn-sign-in">CREATE ACCOUNT</button>
							</div>
							<div class="col-lg-12">
								<p class="grey">By creating an account, you accept the terms of Saptodjojokartiko’s <a href="" class="black">Privacy Policy</a></p>
							</div>
						</div>
					</div>
				</div>
			</form>
			
		</div>
	</div>


	<script src="<?=base_url()?>assets/front/js/jquery-1.10.2.js"></script>
	<script src="<?=base_url()?>assets/front/js/bootstrap.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/front/js/template.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('a[href="#search"]').on('click', function(event) {                    
				$('#search').addClass('open');
				$('#search > form > input[type="search"]').focus();
			});            
			$('#search, #search button.close').on('click keyup', function(event) {
				if (event.target == this || event.target.className == 'close' || event.keyCode == 27) {
					$(this).removeClass('open');
				}
			});            
		});
	</script>

</body>
</html>