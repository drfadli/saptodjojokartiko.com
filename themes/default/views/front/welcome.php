<!DOCTYPE html>
<html>
<head>
	<?php $this->load->view($this->theme . 'front/include/head'); ?>


</head>
<body>
	<header>
		<?php $this->load->view($this->theme . 'front/include/headeraccount'); ?>
	</header>


	<section id="welcome-section">
		<div class="container">
			<div class="row">

				<div class="col-lg-12">
					<h2>Welcome</h2>
					<p>How would you like to be addressed?</p>
				</div>

				<div class="col-lg-12">
					<form>
						<div class="form-data flex">
							<div class="col-lg-2">
								<div class="form-group">
									<label>TITLE</label>
									<select class="form-control">
										<option>MR.</option>
										<option>MS.</option>
										<option>MRS.</option>
									</select>
								</div>
							</div>
							<div class="col-lg-4">
								<div class="form-group">
									<label>FIRST NAME</label>
									<input type="password" name="" class="form-control" placeholder="FIRST NAME" />
								</div>
							</div>
							<div class="col-lg-4">
								<div class="form-group">
									<label>LAST NAME</label>
									<input type="password" name="" class="form-control" placeholder="LAST NAME" />
								</div>
							</div>
							<div class="col-lg-2">
								<div class="form-group">
									<button class="btn btn-signin">SAVE</button>
								</div>
							</div>
						</div>
					</form>
				</div>

			</div>
		</div>
	</section>

	<section id="thanks">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<h2 class="title">Thank you for<br />creating an account</h2>
				</div>
			</div>
		</div>
		<div class="flex">
			<div class="box-lg bg-grey w-50">
				<a href="">

				</a>
			</div>
			<div class="box-lg bg-grey w-50">
				<a href="">

				</a>
			</div>
		</div>
	</section>

	<section id="recommended">
		<div class="container">
			<div class="row">

				<div class="col-lg-6">
					<h1 class="text-center"><a href="<?=site_url('account/recomendations')?>">Recommended <img src="<?=base_url()?>assets/front/img/chev-right.png"></a></h1>
					<p class="subtitle">We have selected special pieces for you from our new collection.</p>
				</div>
				<div class="col-lg-6">
					<h1 class="text-center"><a href="<?=site_url('account/saved_items')?>">Saved items <img src="<?=base_url()?>assets/front/img/chev-right.png"></a></h1>
					<p class="subtitle">All your favorite pieces in one beautiful place.</p>
				</div>

			</div>
		</div>
	</section>

	<section id="stats">
		<?php $this->load->view($this->theme . 'front/include/account_menu'); ?>
	</section>

	<footer>
		<?php $this->load->view($this->theme . 'front/include/footer'); ?>
	</footer>

	<div id="search"> 
		<span class="close">X</span>
		<form role="search" id="searchform" action="/search" method="get">
			<input value="" name="q" type="search" placeholder="type to search"/>
		</form>
	</div>

	<!-- Modal -->
	<div id="register" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<form>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<div class="register-form">
					<div>
						<h1 class="text-center">Create an Account</h1>
						<div class="row">
							<div class="form-group col-lg-12">
								<label>EMAIL ADDRESS</label>
								<input type="email" name="" class="form-control" />
							</div>
							<div class="form-group col-lg-6">
								<label>PASSWORD</label>
								<input type="email" name="" class="form-control" />
							</div>
							<div class="form-group col-lg-6">
								<label>CONFIRM PASSWORD</label>
								<input type="email" name="" class="form-control" />
							</div>
							<div class="col-lg-12">
								<div class="form-group">
									<div class="flex">
										<div class="checkbox">
											<input type="checkbox" id="subscribe" name="" />
											<label for="subscribe"><span></span></label>
										</div>
										<div class="w-90 remember">
											<p class="black"></p>
											<p class="black">Subscribe to Saptodjojokartiko newsletter.</p>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-12">
								<button class="btn btn-sign-in">CREATE ACCOUNT</button>
							</div>
							<div class="col-lg-12">
								<p class="grey">By creating an account, you accept the terms of Saptodjojokartiko’s <a href="" class="black">Privacy Policy</a></p>
							</div>
						</div>
					</div>
				</div>
			</form>
			
		</div>
	</div>


	<script src="<?=base_url()?>assets/front/js/jquery-1.10.2.js"></script>
	<script src="<?=base_url()?>assets/front/js/bootstrap.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/front/js/template.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('a[href="#search"]').on('click', function(event) {                    
				$('#search').addClass('open');
				$('#search > form > input[type="search"]').focus();
			});            
			$('#search, #search button.close').on('click keyup', function(event) {
				if (event.target == this || event.target.className == 'close' || event.keyCode == 27) {
					$(this).removeClass('open');
				}
			});            
		});
	</script>

</body>
</html>