<!DOCTYPE html>
<html>
<head>
	<?php $this->load->view($this->theme . 'front/include/head'); ?>



</head>
<body>
	<header>
		<?php $this->load->view($this->theme . 'front/include/headeraccount'); ?>

	</header>


	<section id="recommendations-page" class="bg-grey">
		<div class="container-fluid">
			<div class="row">
				<div class="col-12">
					<h1 class="text-center" style="margin: 2px 0px 10px 0px;">Account Settings</h1>
				</div>
				<div class="filter-recommendations">
					<select class="form-control col-lg-4">
						<option>My Account</option>
					</select>
					<select class="form-control col-lg-4">
						<option>Recommended</option>
						<option >My Orders</option>
						<option >Saved Items</option>
						<option>Recommended</option>
						<option selected="">Account Settings</option>
						<option>Address Book</option>
						<option>Wallet</option>
						<option>Sign Out</option>
					</select>
				</div>
			</div>
		</div>
	</section>

	<section id="form-setting">
		<div class="container">
			<div class="row">

				<div class="col-lg-12">
					<div class="row">

						<div class="col-lg-4">
							<h3>Your Information</h3>
						</div>
						<div class="col-lg-8">
							<div class="form-group col-lg-2">
								<label>TITLE</label>
								<select class="form-control">
									<option>MR</option>
								</select>
							</div>
							<div class="form-group col-lg-5">
								<label>FIRST NAME</label>
								<input type="text" name="" class="form-control" />
							</div>
							<div class="form-group col-lg-5">
								<label>LAST NAME</label>
								<input type="text" name="" class="form-control" />
							</div>

							<div class="form-group col-lg-6">
								<label>COUNTRY</label>
								<input type="text" name="" class="form-control" />
							</div>
							<div class="form-group col-lg-6">
								<label>ZIP CODE</label>
								<input type="text" name="" class="form-control" />
							</div>

							<div class="form-group col-lg-4">
								<label>BIRTHDAY</label>
								<input type="text" name="" placeholder="Month" class="form-control" />
							</div>
							<div class="form-group col-lg-4">
								<label></label>
								<input type="text" name="" placeholder="Day" class="form-control" />
							</div>
							<div class="form-group col-lg-4">
								<label></label>
								<input type="text" name="" placeholder="Year" class="form-control" />
							</div>
						</div>

						<div class="col-lg-4">
							<h3>Account Details</h3>
						</div>
						<div class="col-lg-8">
							<div class="form-group col-lg-12">
								<label>EMAIL</label>
								<input type="email" name="" class="form-control" />
							</div>
							<div class="col-lg-12">
								<p><a href="">CHANGE EMAIL</a><span style="padding: 0px 40px;">|</span><a href="">CHANGE PASSWORD</a></p>
							</div>
						</div>

						<div class="col-lg-4">
							<h3>Email Preferences</h3>
						</div>
						<div class="col-lg-8">
							<div class="form-group col-lg-12">
								<div class="flex">
									<div class="checkbox">
										<input type="checkbox" id="c1" name="" />
										<label for="c1"><span></span></label>
									</div>
									<div class="w-90 remember">
										<p class="black"></p>
										<p class="grey">Subscribe to receive Saptodjojokartiko email with access to our latest collections, news and more.</p>
									</div>
								</div>
							</div>
							<div class="col-lg-12">
								<a href="" class="btn btn-sign-in">SAVE CHANGES</a>
							</div>
						</div>

					</div>
				</div>

			</div>
		</div>
	</section>

	<footer>
		<?php $this->load->view($this->theme . 'front/include/footer'); ?>

	</footer>

	<div id="search"> 
		<span class="close">X</span>
		<form role="search" id="searchform" action="/search" method="get">
			<input value="" name="q" type="search" placeholder="type to search"/>
		</form>
	</div>

	<!-- Modal -->
	<div id="register" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<form>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<div class="register-form">
					<div>
						<h1 class="text-center">Create an Account</h1>
						<div class="row">
							<div class="form-group col-lg-12">
								<label>EMAIL ADDRESS</label>
								<input type="email" name="" class="form-control" />
							</div>
							<div class="form-group col-lg-6">
								<label>PASSWORD</label>
								<input type="email" name="" class="form-control" />
							</div>
							<div class="form-group col-lg-6">
								<label>CONFIRM PASSWORD</label>
								<input type="email" name="" class="form-control" />
							</div>
							<div class="col-lg-12">
								<div class="form-group">
									<div class="flex">
										<div class="checkbox">
											<input type="checkbox" id="subscribe" name="" />
											<label for="subscribe"><span></span></label>
										</div>
										<div class="w-90 remember">
											<p class="black"></p>
											<p class="black">Subscribe to Saptodjojokartiko newsletter.</p>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-12">
								<button class="btn btn-sign-in">CREATE ACCOUNT</button>
							</div>
							<div class="col-lg-12">
								<p class="grey">By creating an account, you accept the terms of Saptodjojokartiko’s <a href="" class="black">Privacy Policy</a></p>
							</div>
						</div>
					</div>
				</div>
			</form>
			
		</div>
	</div>


	<script src="<?=base_url()?>assets/js/jquery-1.10.2.js"></script>
	<script src="<?=base_url()?>assets/js/bootstrap.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/js/template.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('a[href="#search"]').on('click', function(event) {                    
				$('#search').addClass('open');
				$('#search > form > input[type="search"]').focus();
			});            
			$('#search, #search button.close').on('click keyup', function(event) {
				if (event.target == this || event.target.className == 'close' || event.keyCode == 27) {
					$(this).removeClass('open');
				}
			});            
		});
	</script>

</body>
</html>