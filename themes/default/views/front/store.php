<!DOCTYPE html>
<html>
<head>
	<?php $this->load->view($this->theme . 'front/include/head'); ?>

</head>
<body>
	<header>
		<?php $this->load->view($this->theme . 'front/include/header'); ?>
	</header>

	<div class="filter">
		<div class="container-fluid">
			<ul class="menu-filter menu-left">
				<li>
					<a href="">Store</a>
				</li>
				<li class="divider"></li>
				<li>
					<a href="">Collections</a>
				</li>
				<li class="divider"></li>
				<li>
					<a href="">Dresses</a>
				</li>
			</ul>
			<ul class="menu-filter menu-right">
				<li>
					<select>
						<option>Categori</option>
						<option>Categori</option>
					</select>
				</li>
				<li class="divider"></li>
				<li>
					<select>
						<option>Filters</option>
						<option>Filters</option>
					</select>
				</li>
				<li class="divider"></li>
				<li>
					<select>
						<option>Short By : <b>Newest</b></option>
						<option>Short By : <b>Newest</b></option>
					</select>
				</li>
			</ul>
		</div>
	</div>

	<section id="product">
		<div class="product-container">
			<div class="col-lg-3 col-md-3 no-padding">
				<div class="flip-container" ontouchstart="this.classList.toggle('hover');">
					<div class="flipper">
						<div class="front">
							<img src="<?=base_url()?>assets/front/img/product.png">
							<a href="" class="icon-fav"><img src="<?=base_url()?>assets/front/img/fav.png"></a>
						</div>
						<div class="back">
							<div class="back-product"><img src="<?=base_url()?>assets/front/img/sapto.png"></div>
							<h4>Stretch Jersey Dress with Crystal Trim</h4>
							<p>$30</p>
							<span><a href="<?=site_url('store/detail')?>" class="btn btn-shop">SHOP</a></span>
							<a href="" class="icon-fav"><img src="<?=base_url()?>assets/front/img/fav.png"></a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 no-padding">
				<div class="flip-container" ontouchstart="this.classList.toggle('hover');">
					<div class="flipper">
						<div class="front">
							<img src="<?=base_url()?>assets/front/img/product.png">
							<a href="" class="icon-fav"><img src="<?=base_url()?>assets/front/img/fav.png"></a>
						</div>
						<div class="back">
							<div class="back-product"><img src="<?=base_url()?>assets/front/img/sapto.png"></div>
							<h4>Stretch Jersey Dress with Crystal Trim</h4>
							<p>$30</p>
							<span><a href="<?=site_url('store/detail')?>" class="btn btn-shop">SHOP</a></span>
							<a href="" class="icon-fav"><img src="<?=base_url()?>assets/front/img/fav.png"></a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 no-padding">
				<div class="flip-container" ontouchstart="this.classList.toggle('hover');">
					<div class="flipper">
						<div class="front">
							<img src="<?=base_url()?>assets/front/img/product.png">
							<a href="" class="icon-fav"><img src="<?=base_url()?>assets/front/img/fav.png"></a>
						</div>
						<div class="back">
							<div class="back-product"><img src="<?=base_url()?>assets/front/img/sapto.png"></div>
							<h4>Stretch Jersey Dress with Crystal Trim</h4>
							<p>$30</p>
							<span><a href="<?=site_url('store/detail')?>" class="btn btn-shop">SHOP</a></span>
							<a href="" class="icon-fav"><img src="<?=base_url()?>assets/front/img/fav.png"></a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 no-padding">
				<div class="flip-container" ontouchstart="this.classList.toggle('hover');">
					<div class="flipper">
						<div class="front">
							<img src="<?=base_url()?>assets/front/img/product.png">
							<a href="" class="icon-fav"><img src="<?=base_url()?>assets/front/img/fav.png"></a>
						</div>
						<div class="back">
							<div class="back-product"><img src="<?=base_url()?>assets/front/img/sapto.png"></div>
							<h4>Stretch Jersey Dress with Crystal Trim</h4>
							<p>$30</p>
							<span><a href="<?=site_url('store/detail')?>" class="btn btn-shop">SHOP</a></span>
							<a href="" class="icon-fav"><img src="<?=base_url()?>assets/front/img/fav.png"></a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 no-padding">
				<div class="flip-container" ontouchstart="this.classList.toggle('hover');">
					<div class="flipper">
						<div class="front">
							<img src="<?=base_url()?>assets/front/img/product.png">
							<a href="" class="icon-fav"><img src="<?=base_url()?>assets/front/img/fav.png"></a>
						</div>
						<div class="back">
							<div class="back-product"><img src="<?=base_url()?>assets/front/img/sapto.png"></div>
							<h4>Stretch Jersey Dress with Crystal Trim</h4>
							<p>$30</p>
							<span><a href="<?=site_url('store/detail')?>" class="btn btn-shop">SHOP</a></span>
							<a href="" class="icon-fav"><img src="<?=base_url()?>assets/front/img/fav.png"></a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 no-padding">
				<div class="flip-container" ontouchstart="this.classList.toggle('hover');">
					<div class="flipper">
						<div class="front">
							<img src="<?=base_url()?>assets/front/img/product.png">
							<a href="" class="icon-fav"><img src="<?=base_url()?>assets/front/img/fav.png"></a>
						</div>
						<div class="back">
							<div class="back-product"><img src="<?=base_url()?>assets/front/img/sapto.png"></div>
							<h4>Stretch Jersey Dress with Crystal Trim</h4>
							<p>$30</p>
							<span><a href="<?=site_url('store/detail')?>" class="btn btn-shop">SHOP</a></span>
							<a href="" class="icon-fav"><img src="<?=base_url()?>assets/front/img/fav.png"></a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 no-padding">
				<div class="flip-container" ontouchstart="this.classList.toggle('hover');">
					<div class="flipper">
						<div class="front">
							<img src="<?=base_url()?>assets/front/img/product.png">
							<a href="" class="icon-fav"><img src="<?=base_url()?>assets/front/img/fav.png"></a>
						</div>
						<div class="back">
							<div class="back-product"><img src="<?=base_url()?>assets/front/img/sapto.png"></div>
							<h4>Stretch Jersey Dress with Crystal Trim</h4>
							<p>$30</p>
							<span><a href="<?=site_url('store/detail')?>" class="btn btn-shop">SHOP</a></span>
							<a href="" class="icon-fav"><img src="<?=base_url()?>assets/front/img/fav.png"></a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 no-padding">
				<div class="flip-container" ontouchstart="this.classList.toggle('hover');">
					<div class="flipper">
						<div class="front">
							<img src="<?=base_url()?>assets/front/img/product.png">
							<a href="" class="icon-fav"><img src="<?=base_url()?>assets/front/img/fav.png"></a>
						</div>
						<div class="back">
							<div class="back-product"><img src="<?=base_url()?>assets/front/img/sapto.png"></div>
							<h4>Stretch Jersey Dress with Crystal Trim</h4>
							<p>$30</p>
							<span><a href="<?=site_url('store/detail')?>" class="btn btn-shop">SHOP</a></span>
							<a href="" class="icon-fav"><img src="<?=base_url()?>assets/front/img/fav.png"></a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="footer-pagination">
			<div class="col-lg-6 col-md-6 text-center">
				<div class="result">
					<p>1 - <span class="bold">60</span> of <span class="bold">1661</span> items</p>
				</div>
			</div>
			<div class="col-lg-6 col-md-6">
				<ul class="pagination">
					<li class="active"><a href="">1</a></li>
					<li><a href="">2</a></li>
					<li><a href="">3</a></li>
					<li><a href="">4</a></li>
					<li><a href="">...</a></li>
					<li><a href="">28</a></li>
					<li><a href="" class="bold next">Next</a></li>
				</ul>
			</div>
		</div>
	</section>

	<footer>
		<?php $this->load->view($this->theme . 'front/include/footer'); ?>
	</footer>

	<div id="search"> 
		<span class="close">X</span>
		<form role="search" id="searchform" action="/search" method="get">
			<input value="" name="q" type="search" placeholder="type to search"/>
		</form>
	</div>

	<!-- Modal -->
	<div id="register" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<form>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<div class="register-form">
					<div>
						<h1 class="text-center">Create an Account</h1>
						<div class="row">
							<div class="form-group col-lg-12">
								<label>EMAIL ADDRESS</label>
								<input type="email" name="" class="form-control" />
							</div>
							<div class="form-group col-lg-6">
								<label>PASSWORD</label>
								<input type="email" name="" class="form-control" />
							</div>
							<div class="form-group col-lg-6">
								<label>CONFIRM PASSWORD</label>
								<input type="email" name="" class="form-control" />
							</div>
							<div class="col-lg-12">
								<div class="form-group">
									<div class="flex">
										<div class="checkbox">
											<input type="checkbox" id="subscribe" name="" />
											<label for="subscribe"><span></span></label>
										</div>
										<div class="w-90 remember">
											<p class="black"></p>
											<p class="black">Subscribe to Saptodjojokartiko newsletter.</p>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-12">
								<button class="btn btn-sign-in">CREATE ACCOUNT</button>
							</div>
							<div class="col-lg-12">
								<p class="grey">By creating an account, you accept the terms of Saptodjojokartiko’s <a href="" class="black">Privacy Policy</a></p>
							</div>
						</div>
					</div>
				</div>
			</form>
			
		</div>
	</div>


	<script src="<?=base_url()?>assets/front/js/jquery-1.10.2.js"></script>
	<script src="<?=base_url()?>assets/front/js/bootstrap.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/front/js/template.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('a[href="#search"]').on('click', function(event) {                    
				$('#search').addClass('open');
				$('#search > form > input[type="search"]').focus();
			});            
			$('#search, #search button.close').on('click keyup', function(event) {
				if (event.target == this || event.target.className == 'close' || event.keyCode == 27) {
					$(this).removeClass('open');
				}
			});            
		});
	</script>

</body>
</html>