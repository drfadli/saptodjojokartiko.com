<nav class="navbar navbar-expand-lg">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed toggle-left" data-toggle="collapse" data-target="#navbar7">
						<input type="checkbox" id="h-icon" class="toggle"/>
						<label for="h-icon" class="hamburger">
							<span class="hamburger-line-black hamburger-line--top"></span>
							<span class="hamburger-line-black hamburger-line--middle"></span>
							<span class="hamburger-line-black hamburger-line--bottom"></span>
						</label>
					</button>
					<a class="navbar-brand" href="<?=site_url()?>"><img src="<?=base_url()?>assets/front/img/logo2.png" alt="Dispute Bills">
					</a>
				</div>

				<div id="navbar7" class="navbar-collapse collapse">
					<ul class="nav navbar-nav navbar-left">
						<li class="nav-item active">
							<a class="nav-link" href="#">SHIPPING TO : INDONESIA <img src="<?=base_url()?>assets/front/img/web-black.png"></a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#">CUSTOMER CARE <img src="<?=base_url()?>assets/front/img/mail-black.png"></a>
						</li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<li class="nav-item active">
							<a class="nav-link" href="#search">Search<img src="<?=base_url()?>assets/front/img/search-black.png"></a>
						</li>
						<li class="nav-item">
							<a class="nav-link dropdown-toggle" type="button" data-toggle="dropdown" data-target="#sign-in" href="#">Login / register <img src="<?=base_url()?>assets/front/img/person-black.png"></a>
							<div id="sign-in">
								<ul class="dropdown-menu sign-in" style="position: absolute;right: -10px;">
									<li class="detail-checkout">
										<div class="desc-sign">
											Create an account for access to
											<ul>
												<li>• Saved items in your wishlist</li>
												<li>• Personalized recommendations.</li>
												<li>• Order delivery updates and return management.</li>
											</ul>
										</div>
									</li>
									<li class="footer-checkout"><a href="<?=site_url('account/signin')?>" class="btn btn-sign-in">SIGN <INS></INS></a></li>
									<li class="footer-checkout"><a href="#" type="button" data-toggle="modal" data-target="#register" class="btn btn-sign-up">CREATE ACCOUNT</a></li>
								</ul>
							</div>
							<div id="sign-in-menu" class="">
								<ul class="dropdown-menu" style="position: absolute;right: -10px;">
									<li><a href="" class="header-checkout">My Account</a></li>
									<li class="divider"></li>
									<li><a href="">My Orders</a></li>
									<li class="divider"></li>
									<li><a href="">Saved Items <img src="assets/img/fav.png"></a></li>
									<li class="divider"></li>
									<li><a href="">Recommended</a></li>
									<li class="divider"></li>
									<li><a href="">Account Settings</a></li>
									<li class="divider"></li>
									<li><a href="">Address Book</a></li>
									<li class="divider"></li>
									<li><a href="">Wallet</a></li>
									<li class="divider"></li>
									<li><a href="">Sign out</a></li>
								</ul>
							</div>
						</li>
						<li class="nav-item">
							<a class="nav-link dropdown-toggle" type="button" data-toggle="dropdown" href="#">Shopping bag <img src="<?=base_url()?>assets/front/img/cart-black.png"></a>
							<ul class="dropdown-menu" style="position: absolute;right: 15px;">
								<li class="header-checkout"><h5>SHOOPING BAG</h5></li>
								<li class="divider"></li>
								<li class="detail-checkout">
									<div class="img-checkout">
										<img src="<?=base_url()?>assets/front/img/baju.png">
									</div>
									<div class="desc-checkout">
										<p class="title-desc-checkout">Stretch Jersey Dress with Crystal Trim</p>
										<p>Style xxx xxxxx xxx</p>
										<p>Qty : 1</p>
										<p>Price : $ 3,400</p>
										<p>Size: Small</p>
									</div>
								</li>
								<li class="divider"></li>
								<li class="price-checkout">
									<p class="text-left">Subtotal</p>
									<h4 class="text-right">$3,400</h4>
								</li>
								<li class="divider"></li>
								<li class="footer-checkout"><a href="<?=site_url('checkout')?>" class="btn btn-checkout">checkout</a></li>
							</ul>
						</li>
					</ul>
					<div id="navbar-primary">
						<ul class="nav navbar-nav">
							<li class="nav-item active">
								<a class="nav-link" href="<?=site_url('store')?>">STORE</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="<?=site_url('news')?>">NEWS & STORIES</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="<?=site_url('collection')?>">COLLECTION</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="<?=site_url('gift')?>">BRIDE</a>
							</li>
						</ul>
					</div>
				</div><!-- /.navbar-collapse -->
				<!--/.nav-collapse -->
			</div>
			<!--/.container-fluid -->
		</nav>