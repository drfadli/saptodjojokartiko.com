<div class="container">
			<div class="row">
				<div class="col-lg-1 col-md-1 col-xs-3">
					<ul>
						<li><a href="<?=site_url('store')?>">Store</a></li>
						<li><a href="<?=site_url('page/about')?>">About</a></li>
						<li><a href="">Collection</a></li>
					</ul>
				</div>
				<div class="col-lg-1 col-md-1 col-xs-3">
					<ul>
						<li><a href="">Gift</a></li>
						<li><a href="<?=site_url('news')?>">News</a></li>
						<li><a href="<?=site_url('page/contact')?>">Contact</a></li>
					</ul>
				</div>
				<div class="col-lg-2 col-md-2 col-xs-6">
					<ul>
						<li><a href="<?=site_url('page/terms')?>">Terms & Condition</a></li>
						<li><a href="<?=site_url('page/privacy')?>">Privacy Policy</a></li>
					</ul>
				</div>
				<div class="col-lg-4 col-md-4 col-xs-12">
					<div class="desc-footer">
						<p>
							Maison Saptodjojokartiko
						</p>
						<p>
							Jl. Vila Sawo Kav. 17 Cipete Utara,
						</p>
						<p>
							Kebayoran Baru, Jakarta Selatan, Indonesia 12150
						</p>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-xs-12">
					<p>Subscribe to our Newsletter</p>
					<form>
						<input type="email" name="" class="subscribe" placeholder="Email Address*">
						<button class="submit"><img src="<?=base_url()?>assets/front/img/arrow.png"></button>
					</form>
					<p>2018. All Right Reserved saptodjojokartiko.com</p>
				</div>
			</div>
</div>