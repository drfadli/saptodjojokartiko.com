<div class="">
			<div class="col-lg-3 box-sm bg-grey">
				<div class="title-container">
					<h2><a href="<?=site_url('account/my_orders')?>">My Orders <img src="<?=base_url()?>assets/front/img/chev-right.png"></a></h2>
					<p class="subtitle text-center">Manage and edit your orders.</p>
				</div>
			</div>
			<div class="col-lg-3 box-sm bg-grey">
				<div class="title-container">
					<h2><a href="<?=site_url('account/setting')?>">Account Settings <img src="<?=base_url()?>assets/front/img/chev-right.png"></a></h2>
					<p class="subtitle text-center">Manage profile and preferences.</p>
				</div>
			</div>
			<div class="col-lg-3 box-sm bg-grey">
				<div class="title-container">
					<h2><a href="<?=site_url('account/address_book')?>">Address Book <img src="<?=base_url()?>assets/front/img/chev-right.png"></a></h2>
					<p class="subtitle text-center">Manage shipping & billing address</p>
				</div>
			</div>
			<div class="col-lg-3 box-sm bg-grey">
				<div class="title-container">
					<h2><a href="<?=site_url('account/wallet')?>">Wallet <img src="<?=base_url()?>assets/front/img/chev-right.png"></a></h2>
					<p class="subtitle text-center">Manage your payment methods.</p>
				</div>
			</div>
		</div>