
      // This example adds a marker to indicate the position of Bondi Beach in Sydney,
      // Australia.
      function initMap() {

        // Create a new StyledMapType object, passing it an array of styles,
        // and the name to be displayed on the map type control.
        var styledMapType = new google.maps.StyledMapType(
          [
          {
            "stylers": [
            {
              "saturation": -100
            }
            ]
          }
          ],
          {name: 'Styled Map'});

        // Create a map object, and include the MapTypeId to add
        // to the map type control.
        var map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: 1.2954800, lng: 103.8552799},
          zoom: 17,
          mapTypeControlOptions: {
            mapTypeIds: ['roadmap', 'satellite', 'hybrid', 'terrain',
            'styled_map']
          }
        });
        
        var image = 'assets/img/marker.png';
        var beachMarker = new google.maps.Marker({
          position: {lat: 1.29530, lng: 103.855219},
          map: map,
          icon: image
        });

        //Associate the styled map with the MapTypeId and set it to display.
        map.mapTypes.set('styled_map', styledMapType);
        map.setMapTypeId('styled_map');
      }